define(['angular', 'app', 'services/utils', 'services/projectData'],
	function (angular, todoApp, utils) {
		var controller = todoApp.controller('tasksHierarchyController',
			['$scope', '$state', '$http', 'utils', 'projectData',
			function ($scope, $state, $http, utils, projectData) {
				// Load related css files
				utils.loadCss('/assets/css/tasks.css');

				var projectId = parseInt($state.params.projectId, 10);

				// You can access the scope of the controller from here
				$scope.tasks = projectData.getTasks(projectId);

				$scope.addTask = function () {
					projectData.addTask($scope.newTask, projectId);
					$scope.newTask.name = '';
				};
			}
		]);
		return controller;
	}
);
