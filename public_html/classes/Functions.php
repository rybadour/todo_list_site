<?php
class Functions
{
	public static function redirect($url, $delay = 0, $message = '')
	{
		global $path;
		// if it's an internal relative path
		if ( !preg_match('/^http:\/\/.+/', $url) )
			$url = $path . $url;
			
		if ($delay == 0 && $message = '')
		{
			header('Location: '.$url);
		}
		else
		{
			header('refresh:'.$delay.';url='.$url);
			echo $message;
		}
		exit;
	}

	public static function redirect_error($url, $errorMsg, $delay = 0, $message = '')
	{
		$_SESSION['error'] = $errorMsg;
		$self::redirect($url, $delay, $message);
	}

	public static function setError($error)
	{
		$_SESSION['errors'][] = $error;
	}

	public static function displayErrors()
	{
		if ( !isset($_SESSION['errors']) )
			return;
			
		echo '<div id="errors">';
		echo '<h3>Errors:</h3>';
		echo '<ul>';
		foreach ($_SESSION['errors'] as $error)
			echo "<li>$error</li>";
		echo '</ul>';
		echo '</div>';
		
		unset( $_SESSION['errors'] );
	}

	public static function encryptCookie($value)
	{
		return 'a8488EXK8wA4rWUZG1r92SoqVbr0Kp5FALhhex51tNHIp41NV0' ^ ($value . "");
	}

	public static function setFlag(&$value, $flag)
	{
		$value = $value | $flag;
	}

	public static function unsetFlag(&$value, $flag)
	{
		$value = $value & ~$flag;
	}

	public static function getMonthName($monthNum)
	{
		switch ($monthNum)
		{
			case 1:
				$monthName = 'January';
				break;
				
			case 2:
				$monthName = 'Febuary';
				break;
				
			case 3:
				$monthName = 'March';
				break;
				
			case 4:
				$monthName = 'April';
				break;
				
			case 5:
				$monthName = 'May';
				break;
				
			case 6:
				$monthName = 'June';
				break;
				
			case 7:
				$monthName = 'July';
				break;
				
			case 8:
				$monthName = 'August';
				break;
				
			case 9:
				$monthName = 'September';
				break;
				
			case 10:
				$monthName = 'October';
				break;
				
			case 11:
				$monthName = 'November';
				break;
				
			case 12:
				$monthName = 'December';
				break;
		}
		
		return $monthName;
	}

	// Generates a random string
	// A modified version of http://www.php.net/manual/en/function.rand.php#90773
	public static function rand_str($length, $allowSameAdjacent = false,
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~`!@#$%^&*()_+-={}|:"<>?,/.;\'[]\\')
	{
		if ( !isset($length) )
			return false;

		// Length of character list
		$chars_length = strlen($chars) - 1;

		// Start our string
		$string = $chars{rand(0, $chars_length)};
	   
		// Generate random string
		$i = 1;
		while ($i != $length)
		{
			// Grab a random character from our list
			$r = $chars{rand(0, $chars_length)};
		   
			// Make sure the same two characters don't appear next to each other
			if ($allowSameAdjacent || $r != $string{$i - 1})
			{
				$string .= $r;
				++$i;
			}
		}
	   
		// Return the string
		return $string;
	}

	public static function rand_str_html($length, $allowSameAdjacent = false,
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~`!@#$%^&*()_+-={}|:<>?,/.;\'[]\\')
	{
		return Functions::rand_str($length, $allowSameAdjacent, $chars);
	}
	
	public static function clampStringLength($string, $length)
	{
		if (strlen($string) > $length)
			$string = substr($string, 0, $length-3).'...';
		
		return $string;
	}
}
?>