<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set('dashboard', 'dashboard(/<project_id>(/<folder_id>))')
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'dashboard',
	));

Route::set('default', '(<controller>(/<action>))')
	->defaults(array(
		'controller' => 'Welcome',
		'action'     => 'index',
	));
