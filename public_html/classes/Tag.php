<?php
class Tag extends Wallop
{
	public function Tag($uniqueValues = null, $uniqueColumns = null, 
						  $requestedColumns = null, $rejectedColumns = null)
	{
		$objectInfo = array();
		$objectInfo['tableName'] = 'Tags';
		$objectInfo['defaultColumnName'] = 'tagId';
		$objectInfo['requestedColumns'] = $requestedColumns;
		$objectInfo['rejectedColumns'] = $rejectedColumns;
		
		$relations = array();
		$relations[] = array('className' => 'User', 'relationTableName' => 'TagsToUsers',
		                     'functionalAlias' => 'Owner');
		$relations[] = array('className' => 'Task', 'relationTableName' => 'TagsToTasks',
		                     'functionalAlias' => 'Tasks');
		
		parent::Wallop($objectInfo, $relations, $uniqueValues, $uniqueColumns);
	}
}
?>