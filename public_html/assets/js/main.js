require.config({
	paths: {
		'angular':           'lib/angular',
		'angularRoute':      'lib/angular-route.min',
		'angularUiRouter': 'lib/angular-ui-router.min'
	},
	shim: {
		'angular': {
			exports: 'angular'
		},
		'angularRoute': ['angular'],
		'angularUiRouter': ['angular']
	},
	priority: [
		"angular"
	]
});

window.name = "NG_DEFER_BOOTSTRAP!";

require(['angular', 'app', 'services/utils', 'all_controllers'], 

	function(angular, todoApp) {
        'use strict';

        var utils = angular.injector(['app']).get('utils');
		// Load application wide less
		utils.loadCss("/assets/css/main.css");

		/* */
        angular.element().ready(function() {
        	angular.resumeBootstrap([todoApp['name']]);
        });
		/* */
	}

);
