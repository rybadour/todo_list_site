<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Task extends ORM
{
	protected $_table_name = "tasks";
	protected $_table_columns = array(
		'id' => null,
		'name' => null,
		'folder_id' => null,
		'parent_task_id' => null
	);

	protected $_belongs_to = array(
		'parentTask' => array(
			'model' => 'task',
			'foreign_key' => 'parent_task_id'
		),
		'project' => array(
			'model' => 'project',
			'foreign_key' => 'project_id'
		)
	);

	protected $_has_many = array(
		'subTasks' => array(
			'model' => 'task',
			'foreign_key' => 'parent_task_id'
		),
	);
}
