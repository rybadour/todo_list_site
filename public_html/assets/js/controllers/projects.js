define(['angular', 'app', 'services/utils', 'services/projectData'],
	function (angular, todoApp) {
		var controller = todoApp.controller('projectsController',
			['$scope', '$state', '$http', 'utils', 'projectData',
			function($scope, $state, $http, utils, projectData) {
				projectData.init();

				utils.loadCss("/assets/css/projects.css");

				var projectId = parseInt($state.params.projectId, 10);

				// You can access the scope of the controller from here
				$scope.projects = projectData.getProjects();

				$scope.addProject = function () {
					projectData.addProject($scope.newProject);
					$scope.newProject = {};
				};

				$scope.countProjectTasks = function (projectId) {
					var project = projectData.getProject(projectId);
					var subCount = function (tasks) {
						var count = 0;
						if (tasks) {
							for (var t in tasks) {
								count++;
								count += subCount(tasks[t].childTasks);
							}
						}
						return count;
					};
					return subCount(project.tasks);
				};

				// Current Project
				$scope.project = projectData.getProject(projectId);
			}
		]);
		return controller;
	}
);
