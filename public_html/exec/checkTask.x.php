<?php
require_once('../global.php');

if ( !isset($_POST['taskId']) || !isset($_POST['checked']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task = new Task($_POST['taskId']);
$folder = $task->getParentFolder(0, 1);
$folder = $folder[0];

if ( !$folder || !$folder->hasOwner(User::getCurrentUser()) )
{
	header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task->isChecked = ($_POST['checked'] == 'true');
$task->commit(false);

echo $task->getId();
?>