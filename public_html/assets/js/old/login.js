// Set all password fields to use chroma-hash

$(document).ready(function()
{
	$("input:password").chromaHash({salt:"You shouldn't be reading this right now!"});
});