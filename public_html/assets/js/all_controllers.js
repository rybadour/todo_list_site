var controllers = [
	'projects',
	'tasks',
	'user',
	'user/register'
];

for (var c in controllers) {
	controllers[c] = 'controllers/' + controllers[c];
}

define(controllers, function () {});
