<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Folder extends ORM
{
	protected $_table_name = "folders";
	protected $_table_columns = array(
		'id' => null,
		'name' => null,
		'project_id' => null,
		'parent_folder_id' => null
	);

	protected $_belongs_to = array(
		'project' => array(
			'model' => 'project',
			'foreign_key' => 'project_id'
		),
		'parentFolder' => array(
			'model' => 'folder',
			'foreign_key' => 'parent_folder_id'
		)
	);

	protected $_has_many = array(
		'tasks' => array(
			'model' => 'task',
			'foreign_key' => 'folder_id'
		),
		'subFolders' => array(
			'model' => 'folder',
			'foreign_key' => 'parent_folder_id'
		)
	);
}
