<?php
User::requireLogin();

class PrivatePage extends PageWrapper
{
	private $content;
	private $additionalMenuItems;

	public function PrivatePage($subtitle = '', $content = '', $description = '', $customCss = array(), $jsFiles = array(),
								  $additionalMenuItems = array())
	{
		$title = 'Todo List - '; // Enter you page's main title
		$title .= $subtitle;

		$keywords = array(/* insert keywords here */);
		$author = 'Ryan Badour'; // Enter your author name here

		$css = array('private.css');
		$css = array_merge( (array)$customCss, $css );
		
		$js = array('private.js');
		$js = array_merge( (array)$jsFiles, $js );
		
		$this->additionalMenuItems = $additionalMenuItems;

		parent::PageWrapper($title, $description, $keywords, $author, $css, $js);

		$this->content = $content;
	}

	protected function renderContent()
	{
		echo <<<HeaderHead
	<div id="page">

		<div id="header">
			<a href="/" id="logo">
			<h1>Frumble</h1>
			</a>


			<div id="topMenu">
				<ul>
HeaderHead;

		foreach ($this->additionalMenuItems as $item)
			echo '<li>'.$item.'</li>';

		echo <<<HeaderFoot
					<li><a href="settings.php">settings</a></li>
					<li><a href="http://frumble.ideascale.com" target="_blank">make a suggestion</a></li>
					<li><a href="exec/logout.x.php">logout</a></li>
				</ul>
			</div>
		
		</div>

HeaderFoot;

		echo <<<ContentHeader
		<div id="content">


ContentHeader;
		
		// Content
		$content = $this->content;
		if ( function_exists($content) )
			$content();
		else
			echo $content;

		echo <<<ContentFooter
		</div>


ContentFooter;

		// Footer
		echo <<<Footer

		<div id="footer">
			<br />
			<hr />
			<p class="copy">
			Copyright &copy; 2010, Ryan Badour
			</p>
		</div>

	</div>

Footer;

		Functions::displayErrors();
	}
}
?>
