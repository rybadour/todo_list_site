<?php
require_once('../global.php');

if ( !isset($_POST['taskId'])    ||
     !isset($_POST['title'])     || 
     !isset($_POST['index'])     ||
	 !isset($_POST['indentLevel']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task = new Task( intval($_POST['taskId']) );
$oldIndex = $task->placementIndex;

$folder = $task->getParentFolder();
$folder = $folder[0];

$user = User::getCurrentUser();
$result = $user->hasFolders($folder);

if ( !$result[ $folder->getId() ] )
{
	header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$newIndex = $_POST['index'];

if ($oldIndex != $newIndex)
{
	$tasks = $folder->getTasks(0, -1, 'placementIndex', 'ASC');
	$numTasks = count($tasks);
	$found = false;
	if ($oldIndex < $newIndex)
	{
		for ($i = 0; $i != $numTasks; ++$i)
		{
			$thisIndex = $tasks[$i]->placementIndex;
			if ($found)
			{
				if ($thisIndex == $newIndex)
					$found = false;
					
				$tasks[$i]->placementIndex -= 1;
				$tasks[$i]->commit(false);
			}
			
			if ($thisIndex == $oldIndex)
				$found = true;
		}
	}
	else if ($oldIndex > $newIndex)
	{
		for ($i = 0; $i != $numTasks; ++$i)
		{
			if ($tasks[$i]->placementIndex == $newIndex)
				$found = true;
		
			if ($found)
			{
				if ($tasks[$i]->placementIndex == $oldIndex)
					$found = false;					
				else
				{
					$tasks[$i]->placementIndex += 1;
					$tasks[$i]->commit(false);
				}
			}
		}
	}
}

$task->title = $_POST['title'];

// Only set the body if there is actual text in it
if ( preg_match('/\S/', $_POST['body']) )
{
	$task->body = $_POST['body'];
	$task->isBodyCollapsed = false;
}
else
{
	$task->body = '';
	$task->isBodyCollapsed = true;
}

$task->placementIndex = $newIndex;
$task->indentLevel = $_POST['indentLevel'];
$task->commit(false);
?>