<?php
require_once('../global.php');

if ( !isset($_POST['taskId'])     || 
     !isset($_POST['bodyCollapsed']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task = new Task($_POST['taskId']);
$task->isBodyCollapsed = ($_POST['bodyCollapsed'] == '1');
$task->commit(false);

echo $task->getId();
?>