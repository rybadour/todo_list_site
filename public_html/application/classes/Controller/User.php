<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller {

	protected $_post;

	public function before()
	{
		$this->_post = json_decode($this->request->body(), true);
	}

	public function action_register()
	{
		$email = Arr::get($this->_post, 'email', null);
		$password = Arr::get($this->_post, 'password', null);

		echo $email.' : '.$password;

		$user = ORM::factory('User');
		$user->email = $email;
		$user->username = $email;
		$user->password = $password;

		try
		{
			$user->save();
		}
		catch (ORM_Validation_Exception $e)
		{
			$message = $e->errors();
		}
		$message = array(
			'email' => $email,
			'user' => $user->username
		);

		$this->response->body(json_encode(array(
			'success' => true,
			'message' => $message,
		)));
	}
} // End of Controller_User
