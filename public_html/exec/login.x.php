<?php
require_once('../global.php');

User::requireAnonymous();

if ( !isset($_POST['submit']) )
	Functions::redirect_error('/', 'Who the fuck do you think you are, some kind of hacker?');

if ( !isset($_POST['username']) || $_POST['username'] == '' )
	Functions::redirect_error('/', 'You must enter a username!');

if ( !isset($_POST['password']) || $_POST['password'] == '' )
	Functions::redirect_error('/', 'You must enter a password!');
	
$username = $_POST['username'];
$password = $_POST['password'];

$_SESSION['fillForm'] = array();
$_SESSION['fillForm']['username'] = $username;
if ( isset($_SESSION['rememberMe']) )
	$_SESSION['fillForm']['rememberMe'] = $_SESSION['rememberMe'];
	
if ( strlen($username) < 5 || strlen($username) > Globals::$max_username_size)
	Functions::redirect_error('/', 'Your username must be between 5 and '.Globals::$max_username_size.' characters!');
	
if ( preg_match(Regexs::getUserNameExp(), $username) == 0 )
{
	exit;
	Functions::redirect_error('/', 'Your username must only contains letters, numbers, `_`, `-`, `.`, `(`, `)`, `[`, or `]`!');
}
	
if ( strlen($password) < 6 || strlen($password) > 40)
	Functions::redirect_error('/', 'Your password must be between 6 and 40 characters!');
	
if ( preg_match(Regexs::getPasswordExp(), $password) == 0 )
	Functions::redirect_error('/', 'You entered some weird characters what is wrong with you!?');


// Assume all values are set properly
$user = new User($username, 'username');
if ( $user->password != $user->hashPassword($password) )
	Functions::redirect_error('/', 'The password you entered does not match with this username!');


// Validation done, log this user in!
unset($_SESSION['fillForm']);

$user->salt     = Functions::rand_str(20);
$user->pepper   = Functions::rand_str(20);
$user->password = $user->hashPassword($password);

if ( isset($_POST['rememberMe']) && $_POST['rememberMe'] == 'true' )
{
	$user->cookieId = Functions::rand_str(40);
	
	setcookie('user', $user->getId(), time()+60*60*24*30);
	setcookie('key', User::hashCookie($user->cookieId), time()+60*60*24*30);
}

$user->commit(false);

$_SESSION['userId'] = $user->getId();

Functions::redirect('dashboard.php');
?>