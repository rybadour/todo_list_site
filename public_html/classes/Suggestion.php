<?php
class Suggestion extends Wallop
{
	public function Suggestion($uniqueValues = null, $uniqueColumns = null, 
						   $requestedColumns = null, $rejectedColumns = null)
	{
		$objectInfo = array();
		$objectInfo['tableName'] = 'Suggestions';
		$objectInfo['defaultColumnName'] = 'suggestionId';
		$objectInfo['requestedColumns'] = $requestedColumns;
		$objectInfo['rejectedColumns'] = $rejectedColumns;
		
		$relations = array();
		$relations[] = array('className' => 'User', 'relationTableName' => 'SuggestionsToUser',
		                     'functionalAlias' => 'User');
		
		parent::Wallop($objectInfo, $relations, $uniqueValues, $uniqueColumns);
	}
}
?>