<?php
require_once('../global.php');
require_once('../lib/recaptchalib.php');

User::requireAnonymous();

$_SESSION['fillForm'] = array();

// Validate the form
if ( !isset($_POST['submit']) )
	Functions::redirect_error('/register.php', 'Who the fuck do you think you are, some kind of hacker?');

// Validate Username
if ( !isset($_POST['username']) || $_POST['username'] == '' )
	Functions::redirect_error('/register.php', 'You must enter a username!');
else
	$_SESSION['fillForm']['username'] = $_POST['username'];

// Validate Email
if ( !isset($_POST['email']) || $_POST['email'] == '' )
	Functions::redirect_error('/register.php', 'You must enter an email!');
else
	$_SESSION['fillForm']['email'] = $_POST['email'];

if ( !isset($_POST['password']) || $_POST['password'] == '' )
	Functions::redirect_error('/register.php', 'You must enter a password!');
	
if ( !isset($_POST['confirmPassword']) || $_POST['confirmPassword'] == '' )
	Functions::redirect_error('/register.php', 'You must enter your password twice!');
	
if ($_POST['confirmPassword'] != $_POST['password'])
	Functions::redirect_error('/register.php', 'The passwords you entered do not match!');
	
$username = $_POST['username'];
$password = $_POST['password'];
$email    = $_POST['email'];

$result = recaptcha_check_answer(Globals::reCaptcha['privateKey'],
								 $_SERVER['REMOTE_ADDR'],
								 $_POST['recaptcha_challenge_field'],
								 $_POST['recaptcha_response_field']);
									 
if ( !$result->is_valid )
{
	$_SESSION['capError'] = $result->error;
	Functions::redirect_error('/register.php', 'You did not complete the recaptcha properly!');
}

if ( strlen($username) < 5 || strlen($username) > Globals::max_username_size)
	Functions::redirect_error('/register.php', 'Your username must be between 5 and '.Globals::max_username_size.' characters!');
	
if ( preg_match(Regexs::getUserNameExp(), $username) == 0 )
	Functions::redirect_error('/register.php', 'Your username must only contains letters, numbers, `_`, `-`, `.`, `(`, `)`, `[`, or `]`!');
	
if ( strlen($password) < 6 || strlen($password) > 40 )
	Functions::redirect_error('/register.php', 'Your password must be between 6 and 40 characters!');
	
if ( preg_match((Regexs::getPasswordExp(), $password) == 0 )
	Functions::redirect_error('/register.php', 'You entered some weird characters what is wrong with you!?');
	
if ( preg_match((Regexs::getEmailExp(), $email) == 0 )
	Functions::redirect_error('/register.php', 'You must enter a proper email address!');

$user = new User($username, 'username');
if ( $user->getId() )
	Functions::redirect_error('/register.php', 'The username you entered already exists!');
	
// The form is now validated, let's make a new user!
unset($_SESSION['fillForm']);

$user = new User();
$user->username = $username;
$user->salt     = Functions::rand_str(20);
$user->pepper   = Functions::rand_str(20);
$user->password = $user->hashPassword($password);
$user->email    = $email;
$user->save();

$_SESSION['userId'] = $user->getId();

Functions::redirect('dashboard.php');
?>
