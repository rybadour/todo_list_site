<?php
class Folder extends Wallop
{
	public function Folder($uniqueValues = null, $uniqueColumns = null, 
						  $requestedColumns = null, $rejectedColumns = null)
	{
		$objectInfo = array();
		$objectInfo['tableName'] = 'Folders';
		$objectInfo['defaultColumnName'] = 'folderId';
		$objectInfo['requestedColumns'] = $requestedColumns;
		$objectInfo['rejectedColumns'] = $rejectedColumns;
		
		$relations = array();
		$relations[] = array('className' => 'Folder', 'relationTableName' => 'FoldersToSubFolders',
							  'otherRelationColumnName' => 'subFolderId', 'functionalAlias' => 'SubFolders');
		$relations[] = array('className' => 'User', 'relationTableName' => 'FoldersToUsers',
		                     'functionalAlias' => 'Owner');
		$relations[] = array('className' => 'Task', 'relationTableName' => 'FoldersToTasks',
		                     'functionalAlias' => 'Tasks', 'dependency' => 'composite');
		
		parent::Wallop($objectInfo, $relations, $uniqueValues, $uniqueColumns);
	}
	
	public function countTasks()
	{
		return count($this->getTasks());
	}
}
?>