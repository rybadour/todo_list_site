<?php
require_once('../global.php');

if ( !isset($_POST['name']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$folder = new Folder();
$folder->name = $_POST['name'];
$folder->commit(false);

$user = User::getCurrentUser();
$user->setFolders($folder);

$user->commit();
echo $folder->getId();
?>