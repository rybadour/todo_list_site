<?php
require_once('../global.php');

User::requireLogin();

if ( isset($_COOKIE['user']) || isset($_COOKIE['key']) )
{
	setcookie('user', '', time() - 1);
	setcookie('key', '', time() - 1);
}
unset($_SESSION['userId']);

Functions::redirect('index.php');

?>