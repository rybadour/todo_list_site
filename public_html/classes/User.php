<?php
class User
{
	public static function hashCookie($cookieId)
	{
		// Hashing salt generated from the user's ip and the user agent var
		$salt  = preg_replace('/\.[0-9]{1,3}\.[0-9]{1,3}$/i', '', $_SERVER['REMOTE_ADDR']);
		$salt .= ' '.$_SERVER['HTTP_USER_AGENT'];
		return hash('ripemd160', $cookieId . sha1($salt));
	}
	
	public static function hashPassword($pass)
	{
		return hash('ripemd160', $this->salt . sha1($pass) . $this->pepper);
	}

	public static function requireLogin()
	{
		$user = User::getCurrentUser();
		if ( !$user )
		{
			Functions::redirect('index.php');
			exit;
		}
	}
	
	public static function requireAnonymous()
	{
		$user = User::getCurrentUser();
		if ( $user )
		{
			Functions::redirect('dashboard.php');
			exit;
		}
	}

	// Static Functions
	public static function getCurrentUser()
	{
		if ( isset($_SESSION['userId']) )
		{
			$user = new User($_SESSION['userId']);
			return $user;
		}

		if ( isset($_COOKIE['user']) && isset($_COOKIE['key']) )
		{
			$user = new User( intval($_COOKIE['user']) );
			
			if ( !$user->getId() )
			{
				return false;
			}

			if ( $_COOKIE['key'] != self::hashCookie($user->cookieId) )
			{
				return false;
			}
			
			$_SESSION['userId'] = $_COOKIE['user'];
			$newCookieId = Functions::rand_str(40);
			$user->cookieId = $newCookieId;
			$user->save();
			
			setcookie('key', self::hashCookie($newCookieId), time()+60*60*24*30);
			
			return $user;
		}
		
		return false;
	}


	private $id = false;
	public $username;
	public $password;
	public $cookieId;
	public $formKey;
	public $email;
	public $joinDate;
	public $isEmailConfirmed;
	public $salt;
	public $pepper;

	public function User($userId = false)
	{
		$this->db = new Database();
		$this->db->connect();
		if ($userId !== false)
		{
			$db->execQuery('SELECT * FROM users WHERE id = ?', $userId);
			$row = $db->getRow();

			$this->id = $row['id'];
			$this->username = $row['username'];
			$this->password = $row['password'];
			$this->cookieId = $row['cookieId'];
			$this->formKey = $row['formKey'];
			$this->email = $row['email'];
			$this->joinDate = $row['joinDate'];
			$this->isEmailConfirmed = $row['isEmailConfirmed'];
			$this->salt = $row['salt'];
			$this->pepper = $row['pepper'];
		}
	}

	public function getId()
	{
		return $id;
	}

	public function save()
	{
		if ($id === false)
			$query = 'INSERT INTO users SET username=?, password=?, cookieId=?, formKey=?, email=?, joinDate=?, isEmailConfirmed=?, salt=?, pepper=?;';
		else
			$query = 'UPDATE';

		$this->db->execQuery($query, $this->username, 
	}
}
?>
