(function($) {

	$.extend(true, window, {
		Dashboard: {
			Tasks: Tasks
		}
	});

	function Tasks() {
		
		var tasks = {};

		function init() {
		}

		function moveAfterTask(task, afterTask) {
		}

		function getAllTasks() {
			var flatTasks = [];
			return flatTasks;
		}

		function getCountTop() {
		}

		function getCount() {
		}

		function getFirst() {
		}

		function getLastTop() {
		}

		function getLast() {
		}
		
		init();

		return {
			moveTask: moveTask,
			getCountTop: getCountTop,
			getCount: getCount,
			getFirst: getFirst,
			getLastTop: getLastTop,
			getLast: getLast
		};
	}

})(jQuery);
