<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Project extends Controller {

	public function action_index()
	{
		$projectId = $this->param('projectId');

		$project = ORM::factory('project', $projectId);
		$tasks = $project->tasks->find_all();

		$view = View::factory('project');
		$view->project = $project;
		$view->tasks = $tasks;

		$this->response->body($view);
	}

} // End Welcome
