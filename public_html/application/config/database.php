<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'default' => array
	(
		'type'       => 'PostgreSQL',
		'connection' => array(
			/**
			 * The following options are available for PDO:
			 *
			 * string   dsn         Data Source Name
			 * string   username    database username
			 * string   password    database password
			 * boolean  persistent  use persistent connections?
			 */
			//'dsn'        => 'pgsql:host=localhost;port=5432;dbname=todo',
			'hostname'   => 'localhost',
			'database'   => 'todo',
			'username'   => 'todo',
			'password'   => 'todothismfudger',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'primary_key'  => 'id',
		'charset'      => 'utf8',
		'caching'      => FALSE,
	)
);
