<?php
require_once('../global.php');

if ( !isset($_POST['title'])     || 
     !isset($_POST['body'])     || 
     !isset($_POST['index'])     || 
	 !isset($_POST['folderId'])  ||
	 !isset($_POST['indentLevel']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task = new Task();
$task->title = $_POST['title'];

// Only set the body if there is actual text in it
if ( preg_match('/\S/', $_POST['body']) )
{
	$task->body = $_POST['body'];
	$task->isBodyCollapsed = false;
}

$task->placementIndex = $_POST['index'];
$task->indentLevel = $_POST['indentLevel'];
$task->commit(false);

$folder = new Folder($_POST['folderId']);

$tasks = $folder->getTasks();
$numTasks = count($tasks);
$found = false;
for ($i = 0; $i != $numTasks; ++$i)
{
	if ($tasks[$i]->placementIndex >= $_POST['index'])
	{
		$tasks[$i]->placementIndex += 1;
		$found = true;
	}
}

if ($found)
	$tasks[] = $task;
else
	$tasks = $task;

$folder->setTasks($tasks);
$folder->commit();

echo $task->getId();
?>