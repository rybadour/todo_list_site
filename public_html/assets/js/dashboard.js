(function($) {
	
	$.extend(true, window, {
		Dashboard: {
			Dashboard: Dashboard
		}
	});

	function Dashboard(rootDomId) {

		var tasks;
		var taskControls;

		function init() {
			$dom = $("#"+rootDomId);
			tasks = new Dashboard.Tasks();
			taskControls = new Dashboard.TaskControls(tasks, $dom);
		}

		init();

		return {
		};
	}

});
