// Globals
var selectedTask = 0;
var taskBeingEdited = null;
var addTaskBodyShortLength = 30;

// Cycles the visual focus of the dashboard to the tasks
function cycleToTasks()
{
	// Only cycle if the section isn't already focus on tasks
	if (focusSection != "tasks")
	{
		// Cancel any actions being performed
		resetAddTask();
		
		// Cycle the focus of the page (visually) to the task section
		$("#foldersDimmer").removeClass("hider");
		
		$("#tasksDimmer").addClass("hider");
		
		focusSection = "tasks";
	}
}

// Selects a given task given it's placementIndex
// The direction parameter is used to skip hidden task (due to collapsing)
function selectTask(task, direction)
{
	var taskIndex = parseInt(task.attr("index"));
	cycleToTasks();

	// If the task is hidden keep moving in the same direction
	if ( task.attr("hidden") == "1" )
	{
		switch (direction)
		{
			case "up":
				selectTask( $("div.task[index='"+ (taskIndex-1) +"']"), direction);
				break;
			
			case "down":
				selectTask( $("div.task[index='"+ (taskIndex+1) +"']"), direction);
				break;
				
			case "right":
				
				break;
				
			case "left":
				
				break;
		}
	}
	else
	{
		// Otherwise select the task
		$(".task > div.selected").removeClass("selected");
		task.children(".taskContent").addClass("selected");
		selectedTask = taskIndex;
	}
}

// Starts the process to add a new task
// Parameters determine where the new task's position is to start in the process
function enableAddTask(direction)
{
	cycleToTasks();
	cancelKey();

	// Hide the add new task button and unhide the addTask
	$("#addTaskButton").addClass("hider");
	$("#addTask").removeClass("hider");
	
	// Initialize the new task data and reveal the input field for the task's title
	var newIndex = selectedTask;
	var pivotTask = $(".task[index='"+selectedTask+"']");
	var newIndent = parseInt(pivotTask.attr("indentlevel"));
	if (direction == "down")
	{
		if (pivotTask.attr("iscollapsed") == "1")
			newIndex = getNextIndexBelowTask(pivotTask) + 1;
		else
		{
			$("#addTaskSubTasks").append( pivotTask.children(".subTasks").children(".task") );
			++newIndex;
		}
			
		pivotTask.after( $("#addTask") );
	}
	else if (direction == "right")
	{
		if (pivotTask.attr("iscollapsed") == "1")
		{
			pivotTask.children(".subTasks").removeClass("hider");
		}
		pivotTask.children(".subTasks").prepend( $("#addTask") );
	
		++newIndex;
		++newIndent;
	}
	else if (direction == "up" || direction == "left")
	{
		pivotTask.before( $("#addTask") );
			
		if (direction == "left" && pivotTask.attr("indentlevel") != "0")
		{
			$("#addTaskSubTasks").append(pivotTask);
			--newIndent;
		}
			
		--newIndex;
	}
	else
	{
		newIndex = getNextTaskIndex();
		newIndent = 0;
	}
	
	$("#addTaskIndex").val( newIndex );
	$("#addTaskIndent").val( newIndent );
	$("#addTaskTitle").focus();
	inputAction = Actions.addTask;
}

// The success callback function of the new task Ajax call
function addTaskSuccess(taskId)
{
	// Increment the index of all tasks above this one
	$(".task").each(function(idx, item) {
		if ( parseInt($(item).attr("index")) >= parseInt($("#addTaskIndex").val()) )
		{
			$(item).attr("index", parseInt($(item).attr("index"))+1);
		}
	});
	
	// Increment the task count for this folder
	var folderCount = $("#currentFolder > .folderContent > .folderCount").get(0);
	var count = parseInt(folderCount.innerHTML);
	folderCount.innerHTML = count+1;

	// Setup the new task to add to the DOM
	var showBody = /\S/.test( $("#addTaskBody").val() );
	
	var newTask = $("<div class=\"task\" "+ 
	                 "index=\""+ $("#addTaskIndex").val() +"\" "+
					 "taskId=\""+taskId+"\" "+
					 "iscollapsed=\"0\" "+
					 "isbodycollapsed=\""+(showBody ? "0" : "1")+"\" "+
					 "indentLevel=\""+ $("#addTaskIndent").val() +"\""+	 
			   ">");
			   
	var taskContent = $("<div class=\"taskContent\" onclick=\"selectTask($(this).parent('.task'))\">");
	taskContent.append($("<img class=\"collapseButton\" onclick=\"collapseToggleTask($(this).parent('.taskContent').parent('.task'))\" src=\"img/noButton.png\" />"));
	taskContent.append($("<span class=\"taskSpacer\">").html(" "));
	taskContent.append($("<input type=\"checkbox\" onChange=\"taskCheckChange($(this).parent('.taskContent').parent('.task'), false)\" />"));
	taskContent.append($("<span class=\"taskTitle\" ondblclick=\"enableEditTask($(this).parent('.taskContent').parent('.task'), 'title')\">")
	       .html($("#addTaskTitle").val()));
	taskContent.append($("<span class=\"editButton\" onclick=\"enableEditTask($(this).parent('.taskContent').parent('.task'), 'title')\">"));
	taskContent.append($("<span class=\"deleteButton\" onclick=\"deleteTask($(this).parent('.taskContent').parent('.task'))\">"));
	
	if (showBody)
	{
		taskContent.append($("<br />"));
		taskContent.append($("<span class=\"bodyCollapseButton bodyCloseButton\" onclick=\"collapseBodyToggleTask($(this).parent('.taskContent').parent('.task'))\">"));
		taskContent.append($("<br />"));
	}
	taskContent.append($("<span class=\"taskBody"+ (showBody ? "" : " hider") +"\" ondblclick=\"enableEditTask($(this).parent('.taskContent').parent('.task'), 'body')\">")
	       .html( showBody ? $("#addTaskBody").val() : "" ));
	
	newTask.append(taskContent);
	newTask.append( $("<div class=\"subTasks\">") );
	
	$("#addTask").before(newTask);
	
	// Remove the "there are no tasks message"
	if (count == 0)
		$("#tasks > p").remove();
	
	// Re-collapse the subTasks of the task the new task is in
	if ( $("#addTaskIndent").val() != "0" && 
	     $("#addTask").parent(".subTasks").parent(".task").attr("iscollapsed") == "1")
	{
		$("#addTask").parent(".subTasks").addClass("hider");
	}
	
	// Give the addTask's subtasks to the new task
	newTask.children(".subTasks").append( $("#addTaskSubTasks").children(".task") );
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	selectTask( newTask );
	
	resetAddTask();
	setTasksDimHeight();
	inputAction = "";
}

// Start the process to edit a task
function enableEditTask(task, focus)
{
	var taskIndex = parseInt(task.attr("index"));
	cycleToTasks();
	cancelKey();

	$("#addTaskButton").addClass("hider");
	$("#addTask").removeClass("hider");
	$("#submitAddTaskButton").html("edit task");
	
	$("#editTaskId").val( task.attr("taskid") );
	$("#addTaskTitle").val(task.children(".taskContent").children(".taskTitle").get(0).innerHTML);
	$("#addTaskBody").val(task.children(".taskContent").children(".taskBody").get(0).innerHTML);
	$("#addTaskIndex").val(taskIndex);
	$("#addTaskIndent").val( task.attr("indentlevel") );
	
	task.after( $("#addTask") );
	
	if ( /\S/.test($("#addTaskBody").val()) )
		$("#addTaskBodyContainer").removeClass("hider");
	
	if (focus == 'title')
		$("#addTaskTitle").focus();
	else
	{
		$("#addTaskBodyContainer").removeClass("hider");
		$("#addTaskBody").focus();
	}
	
	$("#addTask").children("#addTaskSubTasks").append( task.children(".subTasks").children(".task") );
	$("#addTask").children(".subTasks").removeClass("hider");
	
	// Reduce the indexes of all tasks above the edited task
	var tasks = getTasksInOrder();
	var end = getNextTaskIndex();
	for (var i = taskIndex + 1; i != end; ++i)
	{
		$(tasks[i]).attr("index", parseInt($(tasks[i]).attr("index")) - 1);
	}
	
	inputAction = Actions.editTask;
	taskBeingEdited = task;
	task.detach();
}

// The success callback function of the edit task Ajax call
function editTaskSuccess()
{
	var editedTask = taskBeingEdited;
	taskBeingEdited = null;
	var newIndex = parseInt($("#addTaskIndex").val());
	var oldIndex = editedTask.attr("index");
	
	// Modify the indexes of all tasks that need to be changed
	$(".task").each(function(idx, item) {
		if ( parseInt($(item).attr("index")) >= newIndex )
		{
			$(item).attr("index", parseInt($(item).attr("index"))+1);
		}
	});
		
	// The Task's attributes
	editedTask.attr("index", $("#addTaskIndex").val() );
	editedTask.attr("indentLevel", $("#addTaskIndent").val() );
			  
	var title = editedTask.children(".taskContent").children("span.taskTitle").get(0);
	$(title).html($("#addTaskTitle").val());
	
	var body = editedTask.children(".taskContent").children("span.taskBody").get(0);
	
	// Decide if we show the body or not
	var isNewEmpty = !/\S/.test( $("#addTaskBody").val() );
	var isOldEmpty = !/\S/.test( $(body).html() );
	
	// Basically, if either of the body's (new or old) is empty (only contains whitespace)
	// then we show the body based on if the new body is empty
	if ( isNewEmpty || isOldEmpty )
	{
		if ( isNewEmpty )
		{
			$(body).html("");
			$(body).addClass("hider");
			$(body).attr("isbodycollapsed", "1");
			$(body).siblings(".bodyCollapseButton").remove();
			$(body).siblings("br").remove();
		}
		else
		{
			$(body).html( $("#addTaskBody").val() );
			$(body).removeClass("hider");
			$(body).attr("isbodycollapsed", "0");
			$(body).before($("<br />"));
			$(body).before(
				$("<span class=\"bodyCollapseButton bodyCloseButton\" onclick=\"collapseBodyToggleTask($(this).parent('.taskContent').parent('.task'))\">")
			); 
			$(body).before($("<br />"));
		}
	}
	// otherwise we leave the visibility as it was before
	else
	{
		$(body).html( $("#addTaskBody").val() );
	}
	
	$("#addTask").after(editedTask);
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	selectTask( editedTask );

	resetAddTask();
	setTasksDimHeight();
	inputAction = "";
}

// Simply clicks on the currently selected task's checkbox
function checkSelectedTask()
{
	$(".taskContent.selected > input").click();
}

// The checkbox callback for tasks
function taskCheckChange(task, noSuccessCallBack)
{
	cycleToTasks();

	// Stop the success function callback if the flag is set
	var successCallBack = (noSuccessCallBack ? "" : checkTaskSuccess);
	$.ajax({
			url: "exec/checkTask.x.php",
			type: "POST",
			data: "taskId="+ task.attr("taskId")+
			      "&checked="+ task.children(".taskContent").children("input[type='checkbox']").attr("checked"),
			success: successCallBack
	});
}

// The check toggle Ajax success callback function
function checkTaskSuccess(taskId)
{
	// Toggle the checked styling of the task
	var task = $("div.task[taskId='"+taskId+"']");
	var subTasks = task.children(".subTasks").find(".task").children(".taskContent");
	
	if ( task.children(".taskContent").children("input[type='checkbox']").attr("checked") )
	{
		task.children(".taskContent").addClass("checked");
		subTasks.addClass("checked");
		subTasks.children("input[type='checkbox']").attr("checked", "checked");
	}
	else
	{
		task.children(".taskContent").removeClass("checked");
		subTasks.removeClass("checked");
		subTasks.children("input[type='checkbox']").attr("checked", "");
	}
}

// Moves the task current being added around on the list of tasks
function moveTask(direction)
{
	var isTitleFocused = $("#addTaskTitle").is(":focus");

	switch (direction)
	{
		case "right":
			if ( !isTaskIndexTopHere($("#addTaskIndex").val(), $("#addTaskIndent").val()) )
			{
				var subTasks = selectTaskNextAboveAddTask().children(".subTasks");
				subTasks.append( $("#addTask") );
				subTasks.append( $("#addTaskSubTasks > .task") );
				subTasks.removeClass("hider");
				$("#addTaskIndent").val( parseInt($("#addTaskIndent").val()) + 1 );
			}
			
			break;
			
		case "left":
			if ( parseInt($("#addTaskIndent").val()) > 0 && $("#addTaskSubTasks > .task").length == 0 )
			{
				// Get the parent of addTask
				var task = $("#addTask").parent(".subTasks").parent(".task");
				task.after( $("#addTask") );
				
				// Add all siblings below us to our subtasks
				// get a sorted list of siblings
				var siblings = task.children(".subTasks").children(".task").sortElements(
				function (a, b)
				{
					return parseInt($(a).attr("indentlevel")) > parseInt($(b).attr("indentlevel")) ? 1 : -1;
				}).get();
				
				for (var index in siblings)
				{
					sibling = $(siblings[index]);
					if ( parseInt(sibling.attr("index")) >= parseInt($("#addTaskIndex").val()) )
						$("#addTaskSubTasks").append(sibling);
				}
				
				$("#addTaskIndent").val( parseInt($("#addTaskIndent").val()) - 1 );
			}
				
			break;
			
		case "up":
			if ( $("#addTaskIndex").val() == "0" )
			{
				// The task is at the top of the page, loop it around
				// only if it has no children
				if ( $("#addTaskSubTasks > .task").length == 0 )
				{
					$("#tasks").append( $("#addTask") );
					$("#addTaskIndex").val( getNextTaskIndex() );
				}
			}
			else
			{
				var taskAbove = selectTaskFromAddTask(-1);
				
				// If the task above this is indented more than one level down from me don't move up
				if ( parseInt(taskAbove.attr("indentlevel")) <= parseInt($("#addTaskIndent").val()) + 1 )
				{
					// If im not the top of this hier
					if ( !isTaskIndexTopHere($("#addTaskIndex").val(), $("#addTaskIndent").val()) )
					{
						// If the task above me is on the same indent level
						if ( $("#addTaskIndent").val() == taskAbove.attr("indentlevel") )
						{
							// swap us and give him my children
							taskAbove.before( $("#addTask") );
							
							taskAbove.children(".subTasks").append( $("#addTaskSubTasks").children(".task") );
						}
						else
							$("#addTaskSubTasks").prepend( taskAbove );
							
						$("#addTaskIndex").val( parseInt($("#addTaskIndex").val()) - 1 );
					}
					// If the task above me is not the top of its hier and I have no children
					else if ( !isTaskTopHere(taskAbove) && $("#addTaskSubTasks > .task").length == 0 )
					{
						// Then swap us
						getNextSiblingAbove(taskAbove).children(".subTasks")
							.append( $("#addTask") );
						$("#addTaskIndex").val( parseInt($("#addTaskIndex").val()) - 1 );
					}
				}
				
				// If the task above us was hidden then move us again
				moveTask("up");
			}
			
			break;
			
		case "down":
			if ( parseInt($("#addTaskIndex").val()) == getNextTaskIndex() )
			{
				if ( $("#addTaskIndent").val() == "0" )
				{
					$("#addTaskIndex").val(0);
					$("#tasks").prepend( $("#addTask") );
				}
			}
			else
			{
				var taskBelow = selectTaskFromAddTask(1);
				var taskBelowTheTaskBelow = selectTaskFromAddTask(2);
				
				// If I am at the bottom of this hier
				if ( parseInt(taskBelow.attr("indentlevel")) < parseInt($("#addTaskIndent").val()) )
				{
					// and the task below me is only one level above
					if ( parseInt(taskBelow.attr("indentlevel")) == (parseInt($("#addTaskIndent").val()) - 1) )
					{
						// nest me inside it
						taskBelow.children(".subTasks").prepend( $("#addTask") );
						$("#addTaskIndex").val( parseInt($("#addTaskIndex").val()) + 1 );
					}
				}
				// If I have no children
				else if ( parseInt(taskBelow.attr("indentlevel")) == parseInt($("#addTaskIndent").val()) )
				{
					// then swap and give the children of the task below to me
					taskBelow.after( $("#addTask") );
					$("#addTaskSubTasks").append( taskBelow.children(".subTasks").children(".task") );
					
					$("#addTaskIndex").val( parseInt($("#addTaskIndex").val()) + 1 );
				}
				// (im not at the top of this hier AND the task below me has no children)
				else if ( !isTaskIndexTopHere($("#addTaskIndex").val(), $("#addTaskIndent").val()) &&
							(parseInt(taskBelow.attr("indentlevel")) == parseInt(taskBelowTheTaskBelow.attr("indentlevel")) ||
							 taskBelowTheTaskBelow.length == 0)
						 )
				{
					selectTaskNextAboveAddTask().children(".subTasks").append( $(".task[index='"+ $("#addTaskIndex").val() + "']") );
					$("#addTaskIndex").val( parseInt($("#addTaskIndex").val()) + 1 );
				}
				
				// If the task below us was hidden move again
				moveTask("down");
			}
			
			break;
	}
	
	if (isTitleFocused)
		$("#addTaskTitle").focus();
	else
		$("#addTaskBody").focus();
}

function collapseToggleTask(task)
{
	cycleToTasks();
	var thisIndent = parseInt(task.attr("indentLevel"));
	var nextIndent = $(".task[index='"+ (parseInt(task.attr("index")) + 1) +"']").attr("indentlevel");
	if (thisIndent < nextIndent)
	{
		$.ajax({
			url: "exec/collapseToggleTask.x.php",
			type: "POST",
			data: "taskId="+ task.attr("taskId") +
			      "&collapsed="+ (task.attr("iscollapsed") == "0" ? "1" : "0"),
			success: collapseToggleTaskSuccess
		});
	}
}

function collapseToggleTaskSuccess(taskId)
{
	var collapsingTask = $(".task[taskId='"+(taskId)+"']");
	var collapseButton = collapsingTask.children(".taskContent").children(".collapseButton");
	
	
	if ( collapsingTask.attr("iscollapsed") == "0" )
	{
		collapsingTask.attr("iscollapsed", "1");
		collapseButton.attr("src", "img/taskCollapsedButton.png");
		
		collapsingTask.children(".subTasks").addClass("hider");
		// Set hidden to 1 on all children
		collapsingTask.find(".task").attr("hidden", 1);
	}
	else
	{
		collapsingTask.attr("iscollapsed", "0");
		collapseButton.attr("src", "img/taskUncollapsedButton.png");
		
		collapsingTask.children(".subTasks").removeClass("hider");
		// Set hidden to 0 on all children
		collapsingTask.find(".task").attr("hidden", 0);
	}
	
	setTasksDimHeight();
}

function collapseBodyToggleTask(task)
{
	// If the body is empty don't toggle the collapse
	var body = task.children(".taskContent").children(".taskBody").get(0);	
	if ( !/\S/.test($(body).html()) )
		return;
		
	$.ajax({
		url: "exec/collapseBodyToggleTask.x.php",
		type: "POST",
		data: "taskId="+task.attr("taskId")+
			  "&bodyCollapsed="+(task.attr("isbodycollapsed") == "0" ? "1" : "0"),
		success: collapseBodyToggleTaskSuccess
	});
}

function collapseBodyToggleTaskSuccess(taskId)
{
	var task = $(".task[taskId='"+(taskId)+"']");
	
	if ( task.attr("isbodycollapsed") == "0" )
	{
		task.children(".taskContent").children(".taskBody").addClass("hider");
		task.attr("isbodycollapsed", "1");
		task.children(".taskContent").children(".bodyCollapseButton").addClass("bodyOpenButton");
		task.children(".taskContent").children(".bodyCollapseButton").removeClass("bodyCloseButton");
	}
	else
	{
		task.children(".taskContent").children(".taskBody").removeClass("hider");
		task.attr("isbodycollapsed", "0");
		task.children(".taskContent").children(".bodyCollapseButton").addClass("bodyCloseButton");
		task.children(".taskContent").children(".bodyCollapseButton").removeClass("bodyOpenButton");
	}
	
	setTasksDimHeight();
}

function deleteTask(task)
{
	cycleToTasks();
	
	// If this task has children alert the user
	if ( task.children(".subTasks").children(".task").length != 0 )
	{
		var result = confirm("Are you sure you want to delete this task, you will also delete its children!");
		if ( !result )
			return;
	}
		
	$.ajax({
		url: "exec/deleteTask.x.php",
		type: "POST",
		data: "taskId="+task.attr("taskId"),  
		success: deleteTaskSuccess
	});
}

function deleteTaskSuccess(taskId)
{
	var deletedTask = $(".task[taskId='"+taskId+"']");
	var deletedIndex = deletedTask.attr("index");
	
	var tasks = getTasksInOrder();
	var found = false;
	
	var numDeletedTasks = deletedTask.find(".task").length + 1;
	
	// Move the index of all tasks above this one
	found = false;
	for (var index in tasks)
	{
		var task = tasks[index];
			
		if ( $(task).attr("index") == deletedIndex )
			found = true;
			
		if (found)
			$(task).attr("index", parseInt($(task).attr("index")) - numDeletedTasks);
	}
	
	deletedTask.remove();

	var folderCount = $("#currentFolder > .folderContent > .folderCount").get(0);
	var count = parseInt(folderCount.innerHTML);
	folderCount.innerHTML = (count-numDeletedTasks);
	
	// If there are now
	if ( $(folderCount).html() == "0" )
		$("#tasks").append($("<p>").html("There are no tasks in this folder!"));
	else
	{
		var iDeletedIndex = parseInt(deletedIndex);
		selectTask($(".task[index="+( iDeletedIndex == 0 ? 0 : iDeletedIndex - 1 )+"]"));
	}
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	resetAddTask();
	setTasksDimHeight();
}

function getTasksInOrder()
{
	var tasks = new Array();
	$(".task").each( function() {
		tasks[ parseInt($(this).attr("index")) ] = this;
	});
	
	return tasks;
}

function redrawCollapseButtons()
{
	var tasks = getTasksInOrder();
	
	for (var index in tasks)
	{
		var task = tasks[index];
		var collapseButton = $(task).children(".taskContent").children(".collapseButton");
		if ( $(task).children(".subTasks").children(".task").length != 0 )
			collapseButton.attr("src", "img/task"+ ($(task).attr("iscollapsed") == "0" ? "Uncollapsed" : "Collapsed") +"Button.png" );
		else
			collapseButton.attr("src", "img/noButton.png");
	}
	
	setTasksDimHeight();
}

function cycleAddTaskFocus(fromClick)
{
	if ( $("#addTaskTitle").is(":focus") || fromClick )
	{
		$("#addTaskOpenBody").addClass("hider");
		$("#addTaskBodyContainer").removeClass("hider");
		$("#addTaskBody").focus();
		
		$("#addTaskTitle").blur();
	}
	else
	{
		$("#addTaskTitle").focus();
		
		$("#addTaskBody").blur();
		// Simple test to check if the body contains only whitespace
		if ( !/\S/.test( $("#addTaskBody").val() ) )
		{
			$("#addTaskBody").val("");
			$("#addTaskOpenBody").html("- add body");
		}
		else
		{
			var body = $("#addTaskBody").val().substring(0, addTaskBodyShortLength);
			$("#addTaskOpenBody").html( (body == $("#addTaskBody").val() ? body : body + "...") );
		}
		
		$("#addTaskOpenBody").removeClass("hider");
		$("#addTaskBodyContainer").addClass("hider");
	}
}

// Finds and returns the next task index (used for adding new tasks)
function getNextTaskIndex()
{
	var max = -1;
	
	$('div.task').each( function(idx, item) {
		if (parseInt($(item).attr("index")) > max)
			max = parseInt($(item).attr("index"));
	});
	
	return  max+1;
}

function getLastIndexBelowTask(task)
{
	var subTasks = $(task).children(".subtasks").find(".task").get();
	
	var max = 0;
	
	for (var index in subTasks)
	{
		if (max < parseInt($(subTasks[index]).attr("index")) )
			max = parseInt($(subTasks[index]).attr("index"));
	}
	
	return max;
}

function isTaskTopHere(task)
{
	// Return true if the task is at the upper hier and is the first task
	if ( $(task).attr("index") == "0" )
		return true;
	
	// Return true if the task above this task is it's parent
	var lastIndent = parseInt($(".task[index='"+ (parseInt($(task).attr("index")) - 1) +"']").attr("indentlevel"));
	return parseInt($(task).attr("indentlevel")) > lastIndent;
}

function isTaskIndexTopHere(index, indentLevel)
{
	index = parseInt(index);
	indentLevel = parseInt(indentLevel);
	
	if (index == 0)
		return true;
		
	return parseInt($(".task[index='"+ (index - 1) +"']").attr("indentLevel")) < indentLevel;
}

function selectTaskFromAddTask(offset)
{
	offset = parseInt(offset);
	if (offset > 0)
		--offset;
	
	if ( $("#editTaskId").val() != "" )
	{
		// remember the direction and get the absolute value of offset
		var direction = (offset > 0 ? 1 : -1);
		offset *= direction;
		var taskId = $("#editTaskId").val();
		
		for (var i = 1; i < offset; ++i)
		{
			if ( taskId == $(".task[index='"+ (parseInt($("#addTaskIndex").val()) + (i * direction)) +"']").attr("taskid") )
			{
				++offset;
				break;
			}
		}
		
		offset *= direction;
	}
		
	return $(".task[index='"+ (parseInt($("#addTaskIndex").val()) + offset) +"']");
}

function selectTaskNextAboveAddTask()
{
	index = parseInt( parseInt($("#addTaskIndex").val()) );
	var parent;
	if ( $("#addTaskIndent").val() == "0" )
		parent = $("#tasks");
	else
		parent = $("#addTask").parent(".subTasks");
		
	var task;
	for (var i = index - 1; i != -1; --i)
	{
		task = parent.children(".task[index='"+i+"']")
		if ( task.length != 0 )
			return task;
	}
}

function getNextSiblingAbove(task)
{
	index = parseInt(task.attr("index"));
	indent = task.attr("indentlevel");
	
	// Loop upwards in the index until we reach the top or 
	do 
	{
		--index;   
		nextTask = $(".task[index="+index+"]");
	}
	while ( index != 0 && nextTask.attr("indentlevel") != indent );
	
	return nextTask.attr("indentlevel") == indent ? nextTask : null;
}

function resetAddTask()
{
	$("#addTaskIndent").val(0);
	$("#addTaskTitle").val("");
	$("#addTaskIndex").val("");
	$("#addTaskBody").val("");
	$("#submitAddTaskButton").html("add task");
	
	$("#addTaskButton").removeClass("hider");
	$("#addTask").addClass("hider");
	
	$("#addTaskOpenBody").removeClass("hider");
	$("#addTaskBodyContainer").addClass("hider");
	
	reformTaskHier();
	
	$("#addTaskButton").appendTo("#tasks");
	$("#addTask").appendTo("#tasks");
}

function reformTaskHier(parentTask)
{
	var indexOffset = 0;
	var startIndex = (parentTask == null ? 0 : parseInt($(parentTask).attr("index")) + 1);
	var currTask;
	var nextTask;
	
	do
	{
		currTask = $(".task[index='"+ (startIndex + indexOffset)  +"']");
		nextTask = $(".task[index='"+ (startIndex + indexOffset + 1)  +"']");
		
		if (parentTask == null)
			$("#tasks").append( currTask );
		else
			parentTask.children(".subTasks").append( currTask );
		
		if ( currTask.attr("indentlevel") < nextTask.attr("indentlevel") )
		{
			indexOffset += reformTaskHier(currTask) + 1;
			nextTask = $(".task[index='"+ (startIndex + indexOffset)  +"']");
		}
		else
			++indexOffset;
		
	} while ( nextTask.length != 0 && currTask.attr("indentlevel") <= nextTask.attr("indentlevel") );
	
	return indexOffset;
}