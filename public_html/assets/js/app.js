define(['angular', 'angularRoute', 'angularUiRouter'],

	function (angular, angularRoute) {
		// Declare app level module which depends on filters, and services

		var app = angular.module('app', ['ui.router']);

        app.config(['$urlRouterProvider', '$stateProvider',
			function($r, $s) {
				// Url Routes and params
				$r.when('/user', '/user/register');
				$r.when('/', '/dashboard');
				$r.when('/projects', '/project/1/tasks');
				$r.otherwise('/dashboard');

				// User Auth:w
				$s.state('user', {
					abstract:    true,
					url:         '/user',
					templateUrl: '/assets/views/user.html',
					controller:  'userController',
				});
				$s.state('user.register', {
					url:         '/register',
					templateUrl: '/assets/views/user/register.html',
					controller:  'registerUserController',
				});

				// Nested states
				$s.state('home', {
					url:         '/dashboard',
					templateUrl: '/assets/views/dashboard.html',
					controller:  'dashBoardController',
				});

				$s.state('project', {
					abstract: true,
					url: '/project/{projectId:[0-9]+}',
					templateUrl: '/assets/views/project.html',
					controller: 'projectsController'
				});

				// Loads the task hierarchy into the root unnamed view of the project template
				$s.state('project.tasks', {
					url: '/tasks',
					templateUrl: '/assets/views/tasks_hierarchy.html',
					controller: 'tasksHierarchyController'
				});
        	}
		]);

		return app;
	}

);
