// Globals
var selectedFolder = 0;

function cycleToFolders()
{
	if (focusSection != "folders")
	{
		cancelKey();
		
		$("#tasksDimmer").removeClass("hider");
		
		$("#foldersDimmer").addClass("hider");
		
		focusSection = "folders";
	}
}

function selectFolder(folderIndex)
{
	cycleToFolders();

	$("div.selectedFolder").removeClass("selectedFolder");
	$("div[index='"+folderIndex+"']").addClass("selectedFolder");
	selectedFolder = folderIndex;
}

function enableAddFolder()
{
	cycleToFolders();

	$("#addFolderButton").addClass("hider");
	
	$("#addFolderName").removeClass("hider");
	$("#addFolderName").focus();
	inputAction = Actions.addFolder;
}

function addFolderSuccess(folderId)
{
	newFolder = $("<div class=\"folder\" index=\""+getNextFolderIndex()+"\" folderId=\""+folderId+"\">");
	newFolder.append($("<span class=\"taskCount\">").html("0"));
	newFolder.append($("<span class=\"name\">").html($("#addFolderName").val()));
	$("#changeFolders").append(newFolder);
	cancelKey();
	chooseFolder(newFolder);
	setFoldersDimHeight();
}

// The callback for the onclick event of clicking on folders
function chooseFolder(currentFolder)
{
	$("#folderId").val($(currentFolder).attr('folderid'));

	$("#changeFolders").submit();
}

// Finds and returns the next folder index (used for adding new folders)
function getNextFolderIndex()
{
	var max = 0;
	
	$('#changeFolders > div.folder').each(function(idx, item) {
		if ( parseInt($(item).attr('index')) > max)
			max = parseInt($(item).attr('index'));
	});
	
	return  parseInt(max)+1;
}