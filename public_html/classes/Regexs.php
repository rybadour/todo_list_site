<?php
class Regexs
{
	public static function getUserNameExp()
	{
		return '/^[a-zA-Z0-9_.()\[\]-]{5,40}$/i';
	}

	public static function getEmailExp()
	{
		return '/^[a-z0-9+%._-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i';
	}
	
	public static function getPasswordExp()
	{
		return '/^[0-9a-zA-Z`=\[\]\\\\;\',.\/~!@#$%^&*()_+{}|:"<>?-]{6,40}$/i';
	}
}
?>