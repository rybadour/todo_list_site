<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Project extends ORM
{
	protected $_table_name = "projects";
	protected $_table_columns = array(
		'id' => null,
		'name' => null,
		'root_folder_id' => null
	);

	protected $_belongs_to = array(
		'rootFolder' => array(
			'model' => 'folder',
			'foreign_key' => 'root_folder_id'
		)
	);
}
