<?php
require_once('../global.php');

if ( !isset($_POST['taskId']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$deletedTask = new Task( intval($_POST['taskId']) );

$folder = $deletedTask->getParentFolder();
$folder = $folder[0];

// Make sure the current user actually owns this task/folder
$user = User::getCurrentUser();
$result = $user->hasFolders($folder);
if ( !$result[ $folder->getId() ] )
{
	header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

// Move the index of all tasks below this task
$tasks = $folder->getTasks(0, -1, 'placementIndex', 'ASC');
$numTasks = count($tasks);
$found = false;
$deleteAsChild = false;
foreach ($tasks as $task)
{
	if ($deleteAsChild)
		if ($task->indentLevel <= $deletedTask->indentLevel)
			$deleteAsChild = false;
		else
			$tasksToDelete[] = $task;

	if ($task->placementIndex == $deletedTask->placementIndex)
	{
		$found = true;
		$deleteAsChild = true;
	}
		
	if ($found)
	{
		$task->placementIndex -= 1;
		$task->commit(false);
	}
}

$deletedId = $deletedTask->getId();

$tasksToDelete[] = $deletedTask;

$folder->removeTasks($tasksToDelete);
$folder->commit();

echo $deletedId;
?>