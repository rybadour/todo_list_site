<?php
require_once('../global.php');

if ( !isset($_POST['taskId'])     || 
     !isset($_POST['collapsed']) )
{
    header("HTTP/1.0 403 Internal Server Error", true, 403);
    exit;
}

$task = new Task($_POST['taskId']);
$task->isCollapsed = ($_POST['collapsed'] == '1');
$task->commit(false);

echo $task->getId();
?>