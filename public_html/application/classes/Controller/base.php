<?php defined('SYSPATH') or die('No direct script access.');

/**
 * The base controller which is extended by all other controllers.
 * Contains the base functionality and checks so that
 * the other controllers don't need to worry about them.
 *
 * @package Controller
 * @author Gilles Paquette <gilles@gillespaquette.ca>
 */

class Controller_base extends Controller {

	var $session;

	var $header;
	var $footer;
	var $error;
	var $succesful;

	private $css;
	private $javascript;
	var $title;

	var $wrapper;
	var $content;
	var $wrap;
	var $currentPage;
	var $json;
	var $is_admin;
	var $admin;

	public function before()
	{
		// set up the session
		$this->session = Session::instance();

		// set the header and footer
		$this->header = View::factory('header');
		$this->header->error = $this->get_error();
		$this->header->success = $this->get_success();
		$this->header->is_logged_in = $this->is_logged_in();
		$this->footer = View::factory('footer');

        // setup the javascript files
        $this->javascript = array();
		$this->javascript[] = "/assets/js/jquery.min.js";

		// setup the css files
		$this->css = array();
		$this->css[] = "/assets/less/main.less";

		// check if we should wrap, by default we should
		$this->wrap = true;
		$ajax = $this->request->query('ajax');
		if($ajax != null && $ajax)
		{
			//this was an ajax request, do not wrap
			$this->wrap = false;
			$this->json = array();
		}

		if ($this->wrap)
		{
			$this->wrapper = Twig::factory('base');
		}
	}

	public function after() 
	{
        $this->header->title = "Frumble";
        $this->header->javascript = $this->javascript;
		$this->header->css = $this->css;

		if($this->wrap)
		{
			$this->wrapper->is_admin = $this->is_admin;
			$this->wrapper->header = $this->header;
			$this->wrapper->footer = $this->footer;
			$this->wrapper->content = $this->content;
			$this->response->body($this->wrapper);
		}
		else
		{
			// do not wrap and return as an ajax request
			$this->wrapper->header = '';
			$this->wrapper->footer = '';
			$this->json['content'] = $this->wrapper->render();
			$this->json['title'] = $this->title;
			$json = json_encode($this->json);
			// set it as a json response
			$this->response->headers('Content-type', 'application/json; charset='.Kohana::$charset);
			$this->response->body($json);
		}
	}

	/**
	 * Appends a script to our list of scripts to load on this page
	 * 
	 */
	public function add_script($script)
	{
		$this->javascript[] = $script;
	}

	/**
	 * Appends a stylesheet to our list of css files to load on this page
	 * 
	 */
	public function add_style($style)
	{
		$this->css[] = $style;
	}

	/**
	 * Gets the error from the session, returns false if the error does not
	 * exists.
	 *
	 * @return string|bool
	 */
	private function get_error()
	{
		$error = $this->session->get('errormsg');
		$this->session->set('errormsg', '');
		if( ! isset($error) || empty($error))
		{
			return false;
		}
		return $error;
	}

	/**
	 * Sets an error to be used at the next page load.
	 *
	 * @param string $msg
	 */
	protected function error($msg)
	{
		$this->session->set('errormsg', $msg);
	}

	/**
	 * Gets a success message, returns false if the success does not
	 * exists.
	 *
	 * @return string|bool
	 */
	private function get_success()
	{
		$succ = $this->session->get('successmsg');
		$this->session->set('successmsg', '');
		if( ! isset($succ) || empty($succ))
		{
			return false;
		}
		return $succ;
	}

	/**
	 * Sets a success message to be used at the next page load.
	 *
	 * @param string $msg
	 */
	protected function success($msg)
	{
		$this->session->set('successmsg', $msg);
	}

	/**
	 * Checks if the current user is logged in.
	 *
	 * @return bool
	 */
	protected function is_logged_in()
	{
		$userid = $this->session->get('userid');
		if(isset($userid) AND $userid > 0)
		{
			return true;
		}
		return false;
	}
}
