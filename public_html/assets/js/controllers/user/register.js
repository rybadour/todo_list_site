define(['angular', 'app', 'services/utils'],
	function (angular, todoApp) {
		var controller = todoApp.controller('registerUserController', [
			'$scope', '$state', '$http', 'utils',
			function($scope, $state, $http, utils) {

				$scope.register = function () {
					if ($scope.newPassword != $scope.confirmPassword) {
						alert("Passwords don't match!");
						return;
					}

					$http.post('/user/register', {
						email: $scope.newEmail,
						password: $scope.newPassword
					}).
					success(function (data, status, headers, config) {
						console.log(data);
						alert("REgistered user");
					}).
					error(function () {
						alert("Error");
					});
				};

			}
		]);
		return controller;
	}
);
