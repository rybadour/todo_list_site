// Globals
var focusSection = "tasks";
var Actions = {"addFolder":0, "editFolder":1, "addTask":2, "editTask":3};
var inputAction = '';
var inputGeneratingKeyPress = false;

// Temporary (hopefully) variables to make keydown work like keypress
var pressedA = false;
var pressedB = false;
var pressedC = false;
var pressedD = false;
var pressedE = false;
var pressedTab = false;

var upKeyTime = 0;
var downKeyTime = 0;
var ctrlRightKeyTime = 0;
var ctrlLeftKeyTime = 0;
var ctrlUpKeyTime = 0;
var ctrlDownKeyTime = 0;

// Bind keyboard controls
$(document).bind("keypress", "RETURN", submitKey);
$(document).bind("keydown", "ESC", cancelKey);
$(document).bind("keydown", {combi: "a", disableInInput: true}, addKey);
$(document).bind("keydown", {combi: "B", disableInInput: true}, collapseBodyKey);
$(document).bind("keyup", {combi: "B", disableInInput: true}, collapseBodyKeyStop);
$(document).bind("keydown", {combi: "C", disableInInput: true}, collapseKey);
$(document).bind("keyup", {combi: "C", disableInInput: true}, collapseKeyStop);
$(document).bind("keydown", {combi: "D", disableInInput: true}, deleteKey);
$(document).bind("keyup", {combi: "D", disableInInput: true}, deleteKeyStop);
$(document).bind("keydown", {combi: "E", disableInInput: true}, editKey);
$(document).bind("keydown", "UP", upKey);
$(document).bind("keydown", "DOWN", downKey);
$(document).bind("keydown", "TAB", tabKey);
$(document).bind("keyup", "TAB", tabKeyStop);
$(document).bind("keydown", "Ctrl+RIGHT", ctrlRightKey);
$(document).bind("keydown", "Ctrl+LEFT", ctrlLeftKey);
$(document).bind("keydown", "Ctrl+UP", ctrlUpKey);
$(document).bind("keydown", "Ctrl+DOWN", ctrlDownKey);

// Extends JQuery by implementing a simple way to check if an element is focused
jQuery.extend(jQuery.expr[':'], {
    focus: function(e){
        try{ return e == document.activeElement; }
        catch(err){ return false; }
    }
});

// Initial Settings
$(document).ready(function()
{
	$("#controlsPanel").dialog({ autoOpen: false, title: "Controls", width: 300, height: 500 });
	
	$("#tabs").tabs({ fx: {opacity: "toggle"} });
});


// Keyboard callbacks and other related functions
// ======================================================

// Move up and down in the task or folder list
function upKey()
{
	var now = new Date().getTime();
	if (upKeyTime > now)
		return false;
		
	upKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		var maxIndex = getNextTaskIndex() - 1;
		
		if (selectedTask == 0 || selectedTask > maxIndex || selectedTask < 0)
			selectTask($(".task[index='"+ maxIndex +"']"), "up");
		else
			selectTask( $(".task[index='"+ (selectedTask - 1) +"']"), "up");
	}
	else
	{
		var maxIndex = getNextFolderIndex() - 1;
		
		if (selectedFolder == 0 || selectedFolder > maxIndex || selectedFolder < 0)
			selectFolder($(".folder[index='"+ maxIndex +"']"), "up");
		else
			selectFolder( $(".folder[index='"+ (selectedFolder - 1) +"']"), "up");
	}
	
	return false;
}

function downKey()
{
	var now = new Date().getTime();
	if (downKeyTime > now)
		return false;
		
	downKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		var maxIndex = getNextTaskIndex() - 1;
		
		if (selectedTask == maxIndex || selectedTask > maxIndex || selectedTask < 0)
			selectTask( $(".task[index='0']"), "down");
		else
			selectTask( $(".task[index='"+ (selectedTask + 1) +"']"), "down");
	}
	else
	{
		var maxIndex = getNextFolderIndex() - 1;
		
		if (selectedFolder == maxIndex || selectedFolder > maxIndex || selectedFolder < 0)
			selectFolder( $(".folder[index='0']"), "down");
		else
			selectFolder( $(".folder[index='"+ (selectedFolder + 1) +"']"), "down");
	}
	
	return false;
}

// Add a task or folder
function addKey()
{
	inputGeneratingKeyPress = true;
	if (focusSection == "tasks")
	{
		enableAddTask("none");
	}
	else
	{
		enableAddFolder("none");
	}
}

// Submit adding or editing of a task or folder
function submitKey()
{
	switch (inputAction)
	{
		case Actions.addFolder:
			$.ajax({
				url: "exec/addFolder.x.php",
				type: "POST",
				data: "name="+$("#addFolderName").val(),
				success: addFolderSuccess
			});
			break;
			
		case Actions.addTask:
			$.ajax({
				url: "exec/addTask.x.php",
				type: "POST",
				data: "title="+ $("#addTaskTitle").val() +
				      "&body="+ $("#addTaskBody").val() +
				      "&index="+ $("#addTaskIndex").val() +
					  "&folderId="+ $("#currentFolder").attr("folderId") +
					  "&indentLevel="+ $("#addTaskIndent").val(),
				success: addTaskSuccess
			});
			break;
			
		case Actions.editTask:
			$.ajax({
				url: "exec/editTask.x.php",
				type: "POST",
				data: "taskId="+ $("#editTaskId").val() +
					  "&title="+ $("#addTaskTitle").val() +
					  "&body="+ $("#addTaskBody").val() +
				      "&index="+ $("#addTaskIndex").val() +
					  "&indentLevel="+ $("#addTaskIndent").val(),
				success: editTaskSuccess
			});
			break;
			
		default:
			if (focusSection == "tasks")
			{
				checkSelectedTask();
			}
			else
				chooseFolder( $("div.selectedFolder").get(0) );
	}
}

// Cancel the current action
function cancelKey()
{
	if (inputAction == Actions.addFolder)
	{
		$("#addFolderButton").removeClass("hider");
		$("#addFolderName").addClass("hider");
		$("#addFolderName").val('');
	}
	
	// Tasks
	if (inputAction == Actions.editTask)
	{
		// Readjust the indexes of the tasks
		var tasks = getTasksInOrder();
		var end = getNextTaskIndex();
		for (var i = parseInt(taskBeingEdited.attr("index")); i != end; ++i)
		{
			$(tasks[i]).attr("index", parseInt($(tasks[i]).attr("index")) + 1);
		}
		$("#tasks").append(taskBeingEdited);
		taskBeingEdited = null;
	}
	
	if (inputAction == Actions.addTask || inputAction == Actions.editTask)
		resetAddTask();
	
	inputAction = "";
}

// Press Tab to cycle between the folders and the tasks sections
function tabKey()
{
	if (!pressedTab)
	{
		if (focusSection == "tasks")
		{
			if (inputAction == Actions.addTask || inputAction == Actions.editTask)
				cycleAddTaskFocus();
			else
				cycleToFolders();
		}
		else
			cycleToTasks();
			
		pressedTab = true;
	}
		
	return false;
}
function tabKeyStop()
{
	if (pressedTab)
		pressedTab = false;
		
	return false;
}

// Press Ctrl+Right to move tasks and folders down a level
function ctrlRightKey()
{
	var now = new Date().getTime();
	if (ctrlRightKeyTime > now)
		return false;
		
	ctrlRightKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		if (inputAction == Actions.addTask || inputAction == Actions.editTask)
			moveTask("right");
		else
			enableAddTask("right");
	}
	else
	{
	
	}
	
	return false;
}

// Press Ctrl+Left to move tasks and folders up a level
function ctrlLeftKey()
{
	var now = new Date().getTime();
	if (ctrlLeftKeyTime > now)
		return false;
		
	ctrlLeftKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		if (inputAction == Actions.addTask || inputAction == Actions.editTask)
			moveTask("left");
		else
			enableAddTask("left");
	}
	else
	{
	
	}
	
	return false;
}

// Press Ctrl+Up to move tasks and folders up
function ctrlUpKey()
{
	var now = new Date().getTime();
	if (ctrlUpKeyTime > now)
		return false;
		
	ctrlUpKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		if (inputAction == Actions.addTask || inputAction == Actions.editTask)
			moveTask("up");
		else
			enableAddTask("up");
	}
	else
	{
	
	}
	
	return false;
}

// Press Ctrl+Down to move tasks and folders down
function ctrlDownKey()
{
	var now = new Date().getTime();
	if (ctrlDownKeyTime > now)
		return false;
		
	ctrlDownKeyTime = now + 50;

	if (focusSection == "tasks")
	{
		if (inputAction == Actions.addTask || inputAction == Actions.editTask)
			moveTask("down");
		else
			enableAddTask("down");
	}
	else
	{
	
	}
	
	return false;
}

// Press C to toggle the collapse of a task or folder
function collapseKey()
{
	if (!pressedC)
	{
		if (focusSection == "tasks")
		{
			collapseToggleTask( $(".taskContent.selected").parent(".task") );
		}
		else
		{
		
		}
	
		pressedC = true;
	}
	
	return false;
}
function collapseKeyStop()
{
	if (pressedC)
		pressedC = false;
		
	return false;
}

// Press E to start editing a task or folder
function editKey()
{
	if (!pressedE)
	{
		if (focusSection == "tasks")
		{
			if ( $(".taskContent.selected").parent(".task").length != 0 )
				enableEditTask( $(".taskContent.selected").parent(".task") , 'title' );
		}
		else
		{
		
		}
	}
	
	return false;
}

// Delete key handling functions
function deleteKey()
{
	if (!pressedD)
	{
		if (focusSection == "tasks")
		{
			if ( $(".taskContent.selected").parent(".task").length != 0 )
				deleteTask( $(".taskContent.selected").parent(".task") );
		}
		else
		{

		}
	
		pressedD = true;
	}
	
	return false;
}
function deleteKeyStop()
{
	if (pressedD)
		pressedD = false;
		
	return false;
}

// Delete key handling functions
function collapseBodyKey()
{
	if (!pressedB)
	{
		if (focusSection == "tasks")
		{
			collapseBodyToggleTask( $(".taskContent.selected").parent(".task") );
		}
		else
		{
		
		}
	
		pressedB = true;
	}
	
	return false;
}
function collapseBodyKeyStop()
{
	if (pressedB)
		pressedB = false;
		
	return false;
}

// Checks for hotkey characters in this input
function removeHotkey(input)
{
	if (inputGeneratingKeyPress)
	{
		inputGeneratingKeyPress = false;
		return false;
	}
}

// ======================================================
// End of Keyboard related functions

function toggleControls()
{
	if ( !$("#controlsPanel").dialog("isOpen") )
		showControls();
	else
		hideControls();
}

function showControls()
{
	$("#controlsPanel").dialog("open");
}

function hideControls()
{
	$("#controlsPanel").dialog("close");
}

function cycleToMovementTab(listEle)
{
	$("#actionsTab").addClass("hider");
	$("#movementTab").removeClass("hider");
	
	$("#controlsTabs > li.selected").removeClass("selected");
	$(listEle).addClass("selected");
}

function cycleToActionsTab(listEle)
{
	$("#movementTab").addClass("hider");
	$("#actionsTab").removeClass("hider");
	
	$("#controlsTabs > li.selected").removeClass("selected");
	$(listEle).addClass("selected");
}

$(setFoldersDimHeight);
$(setTasksDimHeight);

function setFoldersDimHeight()
{
	$("#foldersDimmer").height( parseInt($("#folders").height()) + 20 );
}

function setTasksDimHeight()
{
	$("#tasksDimmer").height( $("#taskSection").height() );
}