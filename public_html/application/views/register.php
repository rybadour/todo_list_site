<?php
require_once('global.php');
	
function content()
{
	global $_CONFIG;
?>
	<fieldset id="register">
		<legend>Registration</legend>
		
		<?php
		if ( isset($_SESSION['error']) )
		{
			echo '<p id="error">'.$_SESSION['error'].'</p><br />';
			unset($_SESSION['error']);
		}
		?>
		
		<form action="exec/register.x.php" method="POST">
			<div class="line">
			<label><div class="label">username</div>
			<input type="text" name="username" 
			  <?php echo isset($_SESSION['fillForm']['username']) ? 'value="'.$_SESSION['fillForm']['username'].'"' : '' ?>
			/>
			</label>
			</div>
			
			<div class="line">
			<label><div class="label">password</div> <input type="password" name="password" /></label>
			</div>
			<div class="line">
			<label><div class="label">confirm password</div> <input type="password" name="confirmPassword" /></label>
			</div>
			
			<div class="line">
			<label><div class="label">email</div> <input type="text" name="email" 
			  <?php echo isset($_SESSION['fillForm']['email']) ? 'value="'.$_SESSION['fillForm']['email'].'"' : '' ?>
			/></label>
			</div>
			<br />
			
			<?php 
			$capError = null;
			if ( isset($_SESSION['capError']) )
			{
				$capError = $_SESSION['capError'];			
				unset($_SESSION['capError']);
			}
			
			echo recaptcha_get_html(Globals::$reCaptcha['publicKey'], $capError); 
			?>
			
			<br />
			<input id="submit" type='submit' name="submit" value='Register' />
		</form>
	</fieldset>
<?php

	unset($_SESSION['fillForm']);
}

$page = new PublicPage('Register', 'content', '', 'register.css');
$page->renderPage();
?>