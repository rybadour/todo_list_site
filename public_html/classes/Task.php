<?php
class Task extends Wallop
{
	public function Task($uniqueValues = null, $uniqueColumns = null, 
						  $requestedColumns = null, $rejectedColumns = null)
	{
		$objectInfo = array();
		$objectInfo['tableName'] = 'Tasks';
		$objectInfo['defaultColumnName'] = 'taskId';
		$objectInfo['requestedColumns'] = $requestedColumns;
		$objectInfo['rejectedColumns'] = $rejectedColumns;
		
		$relations = array();
		$relations[] = array('className' => 'Folder', 'relationTableName' => 'FoldersToTasks',
		                     'functionalAlias' => 'ParentFolder');
		$relations[] = array('className' => 'Tag', 'relationTableName' => 'TagsToTasks',
		                     'functionalAlias' => 'Tag');
		
		parent::Wallop($objectInfo, $relations, $uniqueValues, $uniqueColumns);
	}
}
?>