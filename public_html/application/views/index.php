	<div id="explain">
		<p>
		Welcome to my newest creation. Sorry about the mess this version is just a functional demo nothing pretty.<br />
		If you haven't <a href="register.php">register</a> you should, then you can actually do something!
		</p>
		<br />
		
		<p>
		Maybe you just want to make a <a href="http://frumble.ideascale.com" target="_blank">suggestion</a>?
		</p>
	</div>
	<fieldset id="login">
		<legend>Login</legend>
		<?php
			if ( isset($_SESSION['error']) )
			{
				echo '<p id="error">'.$_SESSION['error'].'</p>';
				unset($_SESSION['error']);
			}
		?>
		<form name="login" action="exec/login.x.php" method="POST">
			<label>username:<br />
			<input type="text" name="username" maxlength="<?php echo $_CONFIG['max_username_size'] ?>"
				<?php echo isset($_SESSION['fillForm']['username']) ? 'value="'.$_SESSION['fillForm']['username'].'"' : '' ?> />
			</label>
			<br />
			
			<label>password:<br />
			<input type="password" name="password" maxlength="40" />
			</label>
			<br />
			
			<label>
			<input type="checkbox" name="rememberMe" value="true" <?php echo isset($_SESSION['fillForm']['rememberMe']) ? 'checked="checked"' : '' ?> />
			remember me</label>
			<br />
			
			<input type="submit" name="submit" value="Login" />
		</form>
	</fieldset>
	
	<div>
		<input type="hidden" value="<?php echo Functions::rand_str_html(40) ?>" />
	</div>
