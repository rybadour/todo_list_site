<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

	public function action_index()
	{
		$view = Twig::factory('welcome');
		$this->response->body($view);
	}

	public function action_dashboard()
	{
		$projectId = $this->request->param('project_id', null);
		$folderId = $this->request->param('folder_id', null);

		$project = ORM::factory('project', $projectId);
		$folder = ORM::factory('folder', $folderId);
		if (!$project->loaded())
			$this->redirect(Route::url("default", array("action" => "welcome")));
		if ($folder->loaded() && $folder->project_id !== $project->id)
			$this->redirect(Route::url("dashboard", array("action" => "dashboard", "project_id" => $projectId)));

		if (!$folder->loaded())
			$folder = $project->rootFolder;
		$tasks = $this->getTaskHierarchy($folder);

		$view = Twig::factory('project');
		$view->project = $project;
		$view->tasks = $tasks;
		$this->response->body($view);
	}

	private function getTaskHierarchy($folder)
	{
		$tasks = $folder->tasks->find_all();

		$taskHierarchy = array();
		$taskIndexMap = array();
		foreach ($tasks as $task)
		{
			$parentId = $task->parent_task_id;
			if ($parentId == null)
			{
				if (array_key_exists($task->id, $taskHierarchy))
					$taskHierarchy[$task->id]['task'] = $task;
				else
				{
					$taskHierarchy[$task->id] = array(
						'task' => $task,
						'subTasks' => array()
					);
				}
				$taskIndexMap[$task->id] = array($task->id);
			}
			else
			{

				if (array_key_exists($parentId, $taskIndexMap))
				{
					$taskLevel =& $taskHierarchy;
					foreach ($taskIndexMap[$parentId] as $hid)
					{
						$taskLevel =& $taskLevel[$hid]['subTasks'];
					}

					$taskIndexMap[$task->id] = $taskIndexMap[$parentId];
				}
				else
				{
					if (!array_key_exists($parentId, $taskHierarchy))
					{
						$taskHierarchy[$parentId] = array(
							'task' => null,
							'subTasks' => array()
						);
					}
	
					$taskLevel =& $taskHierarchy[$parentId]['subTasks'];
					$taskIndexMap[$task->id] = array($parentId);
				}

				$taskLevel[$task->id] = array(
					'task' => $task,
					'subTasks' => array()
				);
				if (array_key_exists($task->id, $taskHierarchy))
				{
					$taskLevel[$task->id]['subTasks'] = $taskHierarchy[$task->id]['subTasks'];
					unset($taskHierarchy[$task->id]);
					foreach ($taskLevel[$task->id]['subTasks'] as $subTask)
					{
						foreach (array_reverse($taskIndexMap[$task->id]) as $index)
						{
							array_unshift($taskIndexMap[$subTask['task']->id], $index);
						}
					}
				}

				unset($taskLevel);
				$taskIndexMap[$task->id][] = $task->id;
			}
		}

		return $taskHierarchy;
	}

} // End Welcome
