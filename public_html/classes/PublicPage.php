<?php
// Assumed to ONLY be required by global.php
User::requireAnonymous();

class PublicPage extends PageWrapper
{
	private $content;

	public function PublicPage($subtitle = '', $content = '', $description = '', $customCss = array(), $jsFiles = array())
	{
		$title = 'Todo List - '; // Enter you page's main title
		$title .= $subtitle;

		$keywords = array(/* insert keywords here */);
		$author = 'Ryan Badour'; // Enter your author name here

		$css = array('public.css');
		$css = array_merge( (array)$customCss, $css );
		
		$js = array(/* insert javascript files here */);
		$js = array_merge( (array)$jsFiles, $js );

		parent::PageWrapper($title, $description, $keywords, $author, $css, $js);

		$this->content = $content;
	}

	protected function renderContent()
	{
		echo <<<Header
	<div id="page">

		<div id="header">
		<a href="/" id="logo">
		<h1>Frumble</h1>
		</a>
		</div>


Header;

		echo <<<ContentHeader
		<div id="content">


ContentHeader;
			
		// Content
		$content = $this->content;
		if ( function_exists($content) )
			$content();
		else
			echo $content;

		echo <<<ContentFooter
		</div>


ContentFooter;

        // Footer
		echo <<<Footer
	
		<div id="footer">
			<br />
			<hr />
			<p class="copy">
			Copyright &copy; 2010, Ryan Badour
			</p>
		</div>
	
	</div>

Footer;

		Functions::displayErrors();
	}
}
?>
