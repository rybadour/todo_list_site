// Globals
var selectedFolder = 0;
var folderBeingEdited = null;

// Cycles the visual focus of the dashboard to the folders
function cycleToFolders()
{
	// Only cycle if the section isn't already focus on folders
	if (focusSection != "folders")
	{
		// Cancel any actions being performed
		resetAddFolder();
		
		// Cycle the focus of the page (visually) to the folder section
		$("#tasksDimmer").removeClass("hider");
		
		$("#foldersDimmer").addClass("hider");
		
		focusSection = "folders";
	}
}

// Selects a given folder given it's placementIndex
// The direction parameter is used to skip hidden folder (due to collapsing)
function selectFolder(folder, direction)
{
	var folderIndex = parseInt(folder.attr("index"));
	cycleToFolders();

	// If the folder is hidden keep moving in the same direction
	if ( folder.hasClass("hider") )
	{
		switch (direction)
		{
			case "up":
				selectFolder( $("div.folder[index='"+ (folderIndex-1) +"']"), direction);
				break;
			
			case "down":
				selectFolder( $("div.folder[index='"+ (folderIndex+1) +"']"), direction);
				break;
				
			case "right":
				
				break;
				
			case "left":
				
				break;
		}
	}
	else
	{
		// Otherwise select the folder
		$(".folder > div.selected").removeClass("selected");
		folder.children(".folderContent").addClass("selected");
		selectedFolder = folderIndex;
	}
}

// Starts the process to add a new folder
// Parameters determine where the new folder's position is to start in the process
function enableAddFolder(direction)
{
	cycleToFolders();
	cancelKey();

	// Hide the add new folder button and
	$("#addFolderButton").addClass("hider");
	
	// Initialize the new folder data and reveal the input field for the folder's title
	var newIndex = selectedFolder;
	var pivotFolder = $(".folder[index='"+selectedFolder+"']");
	var newIndent = parseInt(pivotFolder.attr("indentlevel"));
	if (direction == "down")
	{
		if (pivotFolder.attr("iscollapsed") == "1")
			newIndex = getNextIndexBelowFolder(pivotFolder) + 1;
		else
		{
			$("#addFolderSubFolders").append( pivotFolder.children(".subFolders").children(".folder") );
			++newIndex;
		}
			
		pivotFolder.after( $("#addFolder") );
	}
	else if (direction == "right")
	{
		if (pivotFolder.attr("iscollapsed") == "1")
		{
			pivotFolder.children(".subFolders").removeClass("hider");
		}
		pivotFolder.children(".subFolders").prepend( $("#addFolder") );
	
		++newIndex;
		++newIndent;
	}
	else if (direction == "up" || direction == "left")
	{
		pivotFolder.before( $("#addFolder") );
			
		if (direction == "left" && pivotFolder.attr("indentlevel") != "0")
		{
			$("#addFolderSubFolders").append(pivotFolder);
			--newIndent;
		}
			
		--newIndex;
	}
	else
	{
		newIndex = getNextFolderIndex();
		newIndent = 0;
	}
	
	$("#submitAddFolderButton").removeClass("hider");
	$("#cancelAddFolderButton").removeClass("hider");
	
	$("#addFolderIndex").val( newIndex );
	$("#addFolderIndent").val( newIndent );
	$("#addFolderTitle").removeClass("hider");
	$("#addFolderTitle").focus();
	$("#addFolderArrows").removeClass("hider");
	$("#addFolderOpenBody").removeClass("hider");
	inputAction = Actions.addFolder;
}

// The success callback function of the new folder Ajax call
function addFolderSuccess(folderId)
{
	// Increment the index of all folders above this one
	$(".folder").each(function(idx, item) {
		if ( parseInt($(item).attr("index")) >= parseInt($("#addFolderIndex").val()) )
		{
			$(item).attr("index", parseInt($(item).attr("index"))+1);
		}
	});
	
	// Increment the folder count for this folder
	var folderCount = $("#currentFolder > span.folderCount").get(0);
	var count = parseInt(folderCount.innerHTML);
	folderCount.innerHTML = count+1;

	// Setup the new folder to add to the DOM
	var showBody = /\S/.test( $("#addFolderBody").val() );
	
	var newFolder = $("<div class=\"folder\" "+ 
	                 "index=\""+ $("#addFolderIndex").val() +"\" "+
					 "folderId=\""+folderId+"\" "+
					 "iscollapsed=\"0\" "+
					 "isbodycollapsed=\""+(showBody ? "0" : "1")+"\" "+
					 "indentLevel=\""+ $("#addFolderIndent").val() +"\""+	 
			   ">");
			   
	var folderContent = $("<div class=\"folderContent\" onclick=\"selectFolder($(this).parent('.folder'))\">");
	folderContent.append($("<img class=\"collapseButton\" onclick=\"collapseToggleFolder($(this).parent('.folderContent').parent('.folder'))\" src=\"\" />"));
	folderContent.append($("<span class=\"folderSpacer\">").html(" "));
	folderContent.append($("<input type=\"checkbox\" onChange=\"folderCheckChange($(this).parent('.folderContent').parent('.folder'), false)\" />"));
	folderContent.append($("<span class=\"folderTitle\" ondblclick=\"enableEditFolder($(this).parent('.folderContent').parent('.folder'), 'title')\">")
	       .html($("#addFolderTitle").val()));
	folderContent.append($("<span class=\"editButton\" onclick=\"enableEditFolder($(this).parent('.folderContent').parent('.folder'), 'title')\">"));
	folderContent.append($("<span class=\"deleteButton\" onclick=\"deleteFolder($(this).parent('.folderContent').parent('.folder'))\">"));
	
	if (showBody)
	{
		folderContent.append($("<br />"));
		folderContent.append($("<span class=\"bodyCollapseButton bodyCloseButton\" onclick=\"collapseBodyToggleFolder($(this).parent('.folderContent').parent('.folder'))\">"));
		folderContent.append($("<br />"));
	}
	folderContent.append($("<span class=\"folderBody"+ (showBody ? "" : " hider") +"\" ondblclick=\"enableEditFolder($(this).parent('.folderContent').parent('.folder'), 'body')\">")
	       .html( showBody ? $("#addFolderBody").val() : "" ));
	
	newFolder.append(folderContent);
	newFolder.append( $("<div class=\"subFolders\">") );
	
	$("#addFolder").before(newFolder);
	
	// Remove the "there are no folders message"
	if (count == 0)
		$("#folders > p").remove();
	
	// Re-collapse the subFolders of the folder the new folder is in
	if ( $("#addFolderIndent").val() != "0" && 
	     $("#addFolder").parent(".subFolders").parent(".folder").attr("iscollapsed") == "1")
	{
		$("#addFolder").parent(".subFolders").addClass("hider");
	}
	
	// Give the addFolder's subfolders to the new folder
	newFolder.children(".subFolders").append( $("#addFolderSubFolders").children(".folder") );
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	selectFolder( newFolder );
	
	resetAddFolder();
	setFoldersDimHeight();
	inputAction = "";
}

// Start the process to edit a folder
function enableEditFolder(folder, focus)
{
	var folderIndex = parseInt(folder.attr("index"));
	cycleToFolders();
	cancelKey();

	$("#addFolderButton").addClass("hider");
	$("#addFolderTitle").removeClass("hider");
	$("#addFolderArrows").removeClass("hider");
	
	$("#submitAddFolderButton").removeClass("hider");
	$("#submitAddFolderButton").html("edit folder");
	$("#cancelAddFolderButton").removeClass("hider");
	
	$("#editFolderId").val( folder.attr("folderid") );
	$("#addFolderTitle").val(folder.children(".folderContent").children(".folderTitle").get(0).innerHTML);
	$("#addFolderBody").val(folder.children(".folderContent").children(".folderBody").get(0).innerHTML);
	$("#addFolderIndex").val(folderIndex);
	$("#addFolderIndent").val( folder.attr("indentlevel") );
	
	folder.after( $("#addFolder") );
	
	if ( /\S/.test($("#addFolderBody").val()) )
		$("#addFolderBodyContainer").removeClass("hider");
	
	if (focus == 'title')
		$("#addFolderTitle").focus();
	else
	{
		$("#addFolderBodyContainer").removeClass("hider");
		$("#addFolderBody").focus();
	}
	
	$("#addFolder").children("#addFolderSubFolders").append( folder.children(".subFolders").children(".folder") );
	$("#addFolder").children(".subFolders").removeClass("hider");
	
	// Reduce the indexes of all folders above the edited folder
	var folders = getFoldersInOrder();
	var end = getNextFolderIndex();
	for (var i = folderIndex + 1; i != end; ++i)
	{
		$(folders[i]).attr("index", parseInt($(folders[i]).attr("index")) - 1);
	}
	
	inputAction = Actions.editFolder;
	folderBeingEdited = folder;
	folder.detach();
}

// The success callback function of the edit folder Ajax call
function editFolderSuccess()
{
	var editedFolder = folderBeingEdited;
	folderBeingEdited = null;
	var newIndex = parseInt($("#addFolderIndex").val());
	var oldIndex = editedFolder.attr("index");
	
	// Modify the indexes of all folders that need to be changed
	$(".folder").each(function(idx, item) {
		if ( parseInt($(item).attr("index")) >= newIndex )
		{
			$(item).attr("index", parseInt($(item).attr("index"))+1);
		}
	});
		
	// The Folder's attributes
	editedFolder.attr("index", $("#addFolderIndex").val() );
	editedFolder.attr("indentLevel", $("#addFolderIndent").val() );
			  
	var title = editedFolder.children(".folderContent").children("span.folderTitle").get(0);
	$(title).html($("#addFolderTitle").val());
	
	var body = editedFolder.children(".folderContent").children("span.folderBody").get(0);
	
	// Decide if we show the body or not
	var isNewEmpty = !/\S/.test( $("#addFolderBody").val() );
	var isOldEmpty = !/\S/.test( $(body).html() );
	
	// Basically, if either of the body's (new or old) is empty (only contains whitespace)
	// then we show the body based on if the new body is empty
	if ( isNewEmpty || isOldEmpty )
	{
		if ( isNewEmpty )
		{
			$(body).html("");
			$(body).addClass("hider");
			$(body).attr("isbodycollapsed", "1");
			$(body).siblings(".bodyCollapseButton").remove();
			$(body).siblings("br").remove();
		}
		else
		{
			$(body).html( $("#addFolderBody").val() );
			$(body).removeClass("hider");
			$(body).attr("isbodycollapsed", "0");
			$(body).before($("<br />"));
			$(body).before(
				$("<span class=\"bodyCollapseButton bodyCloseButton\" onclick=\"collapseBodyToggleFolder($(this).parent('.folderContent').parent('.folder'))\">")
			); 
			$(body).before($("<br />"));
		}
	}
	// otherwise we leave the visibility as it was before
	else
	{
		$(body).html( $("#addFolderBody").val() );
	}
	
	$("#addFolder").after(editedFolder);
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	selectFolder( editedFolder );

	resetAddFolder();
	setFoldersDimHeight();
	inputAction = "";
}

// Simply clicks on the currently selected folder's checkbox
function checkSelectedFolder()
{
	$(".folderContent.selected > input").click();
}

// The checkbox callback for folders
function folderCheckChange(folder, noSuccessCallBack)
{
	cycleToFolders();

	// Stop the success function callback if the flag is set
	var successCallBack = (noSuccessCallBack ? "" : checkFolderSuccess);
	$.ajax({
			url: "exec/checkFolder.x.php",
			type: "POST",
			data: "folderId="+ folder.attr("folderId")+
			      "&checked="+ folder.children(".folderContent").children("input[type='checkbox']").attr("checked"),
			success: successCallBack
	});
}

// The check toggle Ajax success callback function
function checkFolderSuccess(folderId)
{
	// Toggle the checked styling of the folder
	var folder = $("div.folder[folderId='"+folderId+"']");
	var subFolders = folder.children(".subFolders").find(".folder").children(".folderContent");
	
	if ( folder.children(".folderContent").children("input[type='checkbox']").attr("checked") )
	{
		folder.children(".folderContent").addClass("checked");
		subFolders.addClass("checked");
		subFolders.children("input[type='checkbox']").attr("checked", "checked");
	}
	else
	{
		folder.children(".folderContent").removeClass("checked");
		subFolders.removeClass("checked");
		subFolders.children("input[type='checkbox']").attr("checked", "");
	}
}

// Moves the folder current being added around on the list of folders
function moveFolder(direction)
{
	var isTitleFocused = $("#addFolderTitle").is(":focus");

	switch (direction)
	{
		case "right":
			if ( !isFolderIndexTopHere($("#addFolderIndex").val(), $("#addFolderIndent").val()) )
			{
				var subFolders = selectFolderNextAboveAddFolder().children(".subFolders");
				subFolders.append( $("#addFolder") );
				subFolders.append( $("#addFolderSubFolders > .folder") );
				subFolders.removeClass("hider");
				$("#addFolderIndent").val( parseInt($("#addFolderIndent").val()) + 1 );
			}
			
			break;
			
		case "left":
			if ( parseInt($("#addFolderIndent").val()) > 0 && $("#addFolderSubFolders > .folder").length == 0 )
			{
				// Get the parent of addFolder
				var folder = $("#addFolder").parent(".subFolders").parent(".folder");
				folder.after( $("#addFolder") );
				
				// Add all siblings below us to our subfolders
				// get a sorted list of siblings
				var siblings = folder.children(".subFolders").children(".folder").sortElements(
				function (a, b)
				{
					return parseInt($(a).attr("indentlevel")) > parseInt($(b).attr("indentlevel")) ? 1 : -1;
					
				}).get();
				
				for (var index in siblings)
				{
					sibling = $(siblings[index]);
					if ( parseInt(sibling.attr("index")) >= parseInt($("#addFolderIndex").val()) )
						$("#addFolderSubFolders").append(sibling);
				}
				
				$("#addFolderIndent").val( parseInt($("#addFolderIndent").val()) - 1 );
			}
				
			break;
			
		case "up":
			if ( $("#addFolderIndex").val() == "0" )
			{
				// The folder is at the top of the page, loop it around
				// only if it has no children
				if ( $("#addFolderSubFolders > .folder").length == 0 )
				{
					$("#folders").append( $("#addFolder") );
					$("#addFolderIndex").val( getNextFolderIndex() );
				}
			}
			else
			{
				var folderAbove = selectFolderFromAddFolder(-1);
				
				// If the folder above this is indented more than one level down from me don't move up
				if ( parseInt(folderAbove.attr("indentlevel")) <= parseInt($("#addFolderIndent").val()) + 1 )
				{
					// If im not the top of this hier
					if ( !isFolderIndexTopHere($("#addFolderIndex").val(), $("#addFolderIndent").val()) )
					{
						// If the folder above me is on the same indent level
						if ( $("#addFolderIndent").val() == folderAbove.attr("indentlevel") )
						{
							// swap us and give him my children
							folderAbove.before( $("#addFolder") );
							
							folderAbove.children(".subFolders").append( $("#addFolderSubFolders").children(".folder") );
						}
						else
							$("#addFolderSubFolders").prepend( folderAbove );
							
						$("#addFolderIndex").val( parseInt($("#addFolderIndex").val()) - 1 );
					}
					// If the folder above me is not the top of its hier and I have no children
					else if ( !isFolderTopHere(folderAbove) && $("#addFolderSubFolders > .folder").length == 0 )
					{
						// Then swap us
						getNextSiblingAbove(folderAbove).children(".subFolders")
							.append( $("#addFolder") );
						$("#addFolderIndex").val( parseInt($("#addFolderIndex").val()) - 1 );
					}
				}
			}
			
			break;
			
		case "down":
			if ( parseInt($("#addFolderIndex").val()) == getNextFolderIndex() )
			{
				if ( $("#addFolderIndent").val() == "0" )
				{
					$("#addFolderIndex").val(0);
					$("#folders").prepend( $("#addFolder") );
				}
			}
			else
			{
				var folderBelow = selectFolderFromAddFolder(1);
				var folderBelowTheFolderBelow = selectFolderFromAddFolder(2);
				
				// If I am at the bottom of this hier
				if ( parseInt(folderBelow.attr("indentlevel")) < parseInt($("#addFolderIndent").val()) )
				{
					// and the folder below me is only one level above
					if ( parseInt(folderBelow.attr("indentlevel")) == (parseInt($("#addFolderIndent").val()) - 1) )
					{
						// nest me inside it
						folderBelow.children(".subFolders").prepend( $("#addFolder") );
						$("#addFolderIndex").val( parseInt($("#addFolderIndex").val()) + 1 );
					}
				}
				// If I have no children
				else if ( parseInt(folderBelow.attr("indentlevel")) == parseInt($("#addFolderIndent").val()) )
				{
					// then swap and give the children of the folder below to me
					folderBelow.after( $("#addFolder") );
					$("#addFolderSubFolders").append( folderBelow.children(".subFolders").children(".folder") );
					
					$("#addFolderIndex").val( parseInt($("#addFolderIndex").val()) + 1 );
				}
				// (im not at the top of this hier AND the folder below me has no children)
				else if ( !isFolderIndexTopHere($("#addFolderIndex").val(), $("#addFolderIndent").val()) &&
							(parseInt(folderBelow.attr("indentlevel")) == parseInt(folderBelowTheFolderBelow.attr("indentlevel")) ||
							 folderBelowTheFolderBelow.length == 0)
						 )
				{
					selectFolderNextAboveAddFolder().children(".subFolders").append( $(".folder[index='"+ $("#addFolderIndex").val() + "']") );
					$("#addFolderIndex").val( parseInt($("#addFolderIndex").val()) + 1 );
				}
			}
			
			break;
	}
	
	if (isTitleFocused)
		$("#addFolderTitle").focus();
	else
		$("#addFolderBody").focus();
}

function collapseToggleFolder(folder)
{
	cycleToFolders();
	var thisIndent = parseInt(folder.attr("indentLevel"));
	var nextIndent = $(".folder[index='"+ (parseInt(folder.attr("index")) + 1) +"']").attr("indentlevel");
	if (thisIndent < nextIndent)
	{
		$.ajax({
			url: "exec/collapseToggleFolder.x.php",
			type: "POST",
			data: "folderId="+ folder.attr("folderId") +
			      "&collapsed="+ (folder.attr("iscollapsed") == "0" ? "1" : "0"),
			success: collapseToggleFolderSuccess
		});
	}
}

function collapseToggleFolderSuccess(folderId)
{
	var collapsingFolder = $(".folder[folderId='"+(folderId)+"']");
	var collapseButton = collapsingFolder.children(".folderContent").children(".collapseButton");
	
	
	if ( collapsingFolder.attr("iscollapsed") == "0" )
	{
		collapsingFolder.attr("iscollapsed", "1");
		collapseButton.attr("src", "img/folderCollapsedButton.png");
		
		collapsingFolder.children(".subFolders").addClass("hider");
	}
	else
	{
		collapsingFolder.attr("iscollapsed", "0");
		collapseButton.attr("src", "img/folderUncollapsedButton.png");
		
		collapsingFolder.children(".subFolders").removeClass("hider");
	}
	
	setFoldersDimHeight();
}

function collapseBodyToggleFolder(folder)
{
	// If the body is empty don't toggle the collapse
	var body = folder.children(".folderContent").children(".folderBody").get(0);	
	if ( !/\S/.test($(body).html()) )
		return;
		
	$.ajax({
		url: "exec/collapseBodyToggleFolder.x.php",
		type: "POST",
		data: "folderId="+folder.attr("folderId")+
			  "&bodyCollapsed="+(folder.attr("isbodycollapsed") == "0" ? "1" : "0"),
		success: collapseBodyToggleFolderSuccess
	});
}

function collapseBodyToggleFolderSuccess(folderId)
{
	var folder = $(".folder[folderId='"+(folderId)+"']");
	
	if ( folder.attr("isbodycollapsed") == "0" )
	{
		folder.children(".folderContent").children(".folderBody").addClass("hider");
		folder.attr("isbodycollapsed", "1");
		folder.children(".folderContent").children(".bodyCollapseButton").addClass("bodyOpenButton");
		folder.children(".folderContent").children(".bodyCollapseButton").removeClass("bodyCloseButton");
	}
	else
	{
		folder.children(".folderContent").children(".folderBody").removeClass("hider");
		folder.attr("isbodycollapsed", "0");
		folder.children(".folderContent").children(".bodyCollapseButton").addClass("bodyCloseButton");
		folder.children(".folderContent").children(".bodyCollapseButton").removeClass("bodyOpenButton");
	}
	
	setFoldersDimHeight();
}

function deleteFolder(folder)
{
	cycleToFolders();
	
	// If this folder has children alert the user
	if ( folder.children(".subFolders").children(".folder").length != 0 )
	{
		var result = confirm("Are you sure you want to delete this folder, you will also delete its children!");
		if ( !result )
			return;
	}
		
	$.ajax({
		url: "exec/deleteFolder.x.php",
		type: "POST",
		data: "folderId="+folder.attr("folderId"),  
		success: deleteFolderSuccess
	});
}

function deleteFolderSuccess(folderId)
{
	var deletedFolder = $(".folder[folderId='"+folderId+"']");
	var deletedIndex = deletedFolder.attr("index");
	
	var folders = getFoldersInOrder();
	var found = false;
	
	var numDeletedFolders = deletedFolder.find(".folder").length + 1;
	
	// Move the index of all folders above this one
	found = false;
	for (var index in folders)
	{
		var folder = folders[index];
			
		if ( $(folder).attr("index") == deletedIndex )
			found = true;
			
		if (found)
			$(folder).attr("index", parseInt($(folder).attr("index")) - numDeletedFolders);
	}
	
	deletedFolder.remove();

	var folderCount = $("#currentFolder > span.folderCount").get(0);
	var count = parseInt(folderCount.innerHTML);
	folderCount.innerHTML = (count-numDeletedFolders);
	
	// If there are now
	if ( $(folderCount).html() == "0" )
		$("#folders").append($("<p>").html("There are no folders in this folder!"));
	else
	{
		var iDeletedIndex = parseInt(deletedIndex);
		selectFolder($(".folder[index="+( iDeletedIndex == 0 ? 0 : iDeletedIndex - 1 )+"]"));
	}
	
	// Redraw all the collapse buttons
	redrawCollapseButtons();
	
	resetAddFolder();
	setFoldersDimHeight();
}

function getFoldersInOrder()
{
	var folders = new Array();
	$(".folder").each( function() {
		folders[ parseInt($(this).attr("index")) ] = this;
	});
	
	return folders;
}

function redrawCollapseButtons()
{
	var folders = getFoldersInOrder();
	
	for (var index in folders)
	{
		var folder = folders[index];
		var collapseButton = $(folder).children(".folderContent").children(".collapseButton");
		if ( $(folder).children(".subFolders").children(".folder").length != 0 )
			collapseButton.attr("src", "img/folder"+ ($(folder).attr("iscollapsed") == "0" ? "Uncollapsed" : "Collapsed") +"Button.png" );
		else
			collapseButton.attr("src", "");
	}
	
	setFoldersDimHeight();
}

function cycleAddFolderFocus(fromClick)
{
	if ( $("#addFolderTitle").is(":focus") || fromClick )
	{
		$("#addFolderOpenBody").addClass("hider");
		$("#addFolderBodyContainer").removeClass("hider");
		$("#addFolderBody").focus();
		
		$("#addFolderTitle").blur();
	}
	else
	{
		$("#addFolderTitle").focus();
		
		$("#addFolderBody").blur();
		// Simple test to check if the body contains only whitespace
		if ( !/\S/.test( $("#addFolderBody").val() ) )
		{
			$("#addFolderBody").val("");
			$("#addFolderOpenBody").html("- add body");
		}
		else
			$("#addFolderOpenBody").html( $("#addFolderBody").val().substring(0, addFolderBodyShortLength) + "...");
		
		$("#addFolderOpenBody").removeClass("hider");
		$("#addFolderBodyContainer").addClass("hider");
	}
}

// Finds and returns the next folder index (used for adding new folders)
function getNextFolderIndex()
{
	var max = -1;
	
	$('div.folder').each( function(idx, item) {
		if (parseInt($(item).attr("index")) > max)
			max = parseInt($(item).attr("index"));
	});
	
	return  max+1;
}

function getLastIndexBelowFolder(folder)
{
	var subFolders = $(folder).children(".subfolders").find(".folder").get();
	
	var max = 0;
	
	for (var index in subFolders)
	{
		if (max < parseInt($(subFolders[index]).attr("index")) )
			max = parseInt($(subFolders[index]).attr("index"));
	}
	
	return max;
}

function isFolderTopHere(folder)
{
	// Return true if the folder is at the upper hier and is the first folder
	if ( $(folder).attr("index") == "0" )
		return true;
	
	// Return true if the folder above this folder is it's parent
	var lastIndent = parseInt($(".folder[index='"+ (parseInt($(folder).attr("index")) - 1) +"']").attr("indentlevel"));
	return parseInt($(folder).attr("indentlevel")) > lastIndent;
}

function isFolderIndexTopHere(index, indentLevel)
{
	index = parseInt(index);
	indentLevel = parseInt(indentLevel);
	
	if (index == 0)
		return true;
		
	return parseInt($(".folder[index='"+ (index - 1) +"']").attr("indentLevel")) < indentLevel;
}

function selectFolderFromAddFolder(offset)
{
	offset = parseInt(offset);
	if (offset > 0)
		--offset;
	
	if ( $("#editFolderId").val() != "" )
	{
		// remember the direction and get the absolute value of offset
		var direction = (offset > 0 ? 1 : -1);
		offset *= direction;
		var folderId = $("#editFolderId").val();
		
		for (var i = 1; i < offset; ++i)
		{
			if ( folderId == $(".folder[index='"+ (parseInt($("#addFolderIndex").val()) + (i * direction)) +"']").attr("folderid") )
			{
				++offset;
				break;
			}
		}
		
		offset *= direction;
	}
		
	return $(".folder[index='"+ (parseInt($("#addFolderIndex").val()) + offset) +"']");
}

function selectFolderNextAboveAddFolder()
{
	index = parseInt( parseInt($("#addFolderIndex").val()) );
	var parent;
	if ( $("#addFolderIndent").val() == "0" )
		parent = $("#folders");
	else
		parent = $("#addFolder").parent(".subFolders");
		
	var folder;
	for (var i = index - 1; i != -1; --i)
	{
		folder = parent.children(".folder[index='"+i+"']")
		if ( folder.length != 0 )
			return folder;
	}
}

function getNextSiblingAbove(folder)
{
	index = parseInt(folder.attr("index"));
	indent = folder.attr("indentlevel");
	
	// Loop upwards in the index until we reach the top or 
	do 
	{
		--index;   
		nextFolder = $(".folder[index="+index+"]");
	}
	while ( index != 0 && nextFolder.attr("indentlevel") != indent );
	
	return nextFolder.attr("indentlevel") == indent ? nextFolder : null;
}

function resetAddFolder()
{
	$("#addFolderIndent").val(0);
	$("#addFolderTitle").addClass("hider");
	$("#addFolderTitle").val("");
	$("#addFolderArrows").addClass("hider");
	$("#addFolderIndex").val("");
	$("#addFolderOpenBody").addClass("hider");
	$("#addFolderBodyContainer").addClass("hider");
	$("#addFolderBody").val("");
	$("#addFolderButton").removeClass("hider");
	$("#submitAddFolderButton").addClass("hider");
	$("#submitAddFolderButton").html("add folder");
	$("#cancelAddFolderButton").addClass("hider");
	
	reformFolderHier();
	
	$("#addFolder").appendTo("#folders");
}

function reformFolderHier(parentFolder)
{
	var indexOffset = 0;
	var startIndex = (parentFolder == null ? 0 : parseInt($(parentFolder).attr("index")) + 1);
	var currFolder;
	var nextFolder;
	
	do
	{
		currFolder = $(".folder[index='"+ (startIndex + indexOffset)  +"']");
		nextFolder = $(".folder[index='"+ (startIndex + indexOffset + 1)  +"']");
		
		if (parentFolder == null)
			$("#folders").append( currFolder );
		else
			parentFolder.children(".subFolders").append( currFolder );
		
		if ( currFolder.attr("indentlevel") < nextFolder.attr("indentlevel") )
		{
			indexOffset += reformFolderHier(currFolder) + 1;
			nextFolder = $(".folder[index='"+ (startIndex + indexOffset)  +"']");
		}
		else
			++indexOffset;
		
	} while ( nextFolder.length != 0 && currFolder.attr("indentlevel") <= nextFolder.attr("indentlevel") );
	
	return indexOffset;
}