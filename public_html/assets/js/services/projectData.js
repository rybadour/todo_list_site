define(['angular', 'app'],
	function (angular, app) {
		return app.service('projectData', function () {
			var projects = [
			];

			this.getProjects = function () {
				return projects;
			};

			this.getProject = function (id) {
				for (var p in projects) {
					if (projects[p].id === id) {
						return projects[p];
					}
				}
				return null;
			};

			this.addProject = function (project, record) {
				var newProject = {
					name: project.name
				};
				projects.push(newProject);

				this.updateCache();
				
				if (record !== false) {
					this.recordAction({
						type: 'addProject',
						context: newProject
					}, Date.now());
				}
			};

			// Tasks
			this.getTasks = function (projectId) {
				var project = this.getProject(projectId);
				return project.tasks;
			};


			// TODO: False by default, true only if request for actions is very large
			var cacheFarBehind = false;

			var cacheLocally = true;

			/**
			 * Contains the client cache of actions the user performed.
			 * Actions should be ordered by the exact time they were performed and be given a unique
			 * token based on the context of the action (check sum idea)
			 */
			var actionQueue = [
			];

			this.init = function () {
				if (cacheLocally) {
					this.getStateFromCache();
				}

				if (cacheFarBehind) {
					this.getNewState();
				} else {
					this.getRemoteActions();
				}
			};

			/**
			 * Retrieve the state from the local browser cache.
			 */
			this.getStateFromCache = function () {
				if (localStorage) {
					if (localStorage['all_projects']) {
						projects = JSON.parse(localStorage['all_projects']);
					}
					
					return true;
				}
				return false;
			};

			/**
			 * Update the local storage cache for our state.
			 */
			this.updateCache = function () {
				if (cacheLocally && localStorage) {
					localStorage['all_projects'] = angular.toJson(projects);

					return true;
				}
				return false;
			};

			/**
			 * Retrieves the whole state from the server. Used only when getting the actions and
			 * applying them is expected to be slower.
			 */
			this.getNewState = function () {
				// TODO: ajax and shit

				// TODO: remove testing
				projects = [
					{id: 1, name: "Urquan", tasks: [
						{
							id: 1,
							name: 'Eat food',
							completed: false,
							childTasks: []
						},
						{
							id: 2,
							name: 'Get Food',
							completed: false,
							childTasks: [
								{id: 4, name: 'Milk', completed: true, childTasks: []},
							]
						},
						{
							id: 3,
							name: 'Sleep',
							completed: true,
							childTasks: []
						}
					]},
					{id: 2, name: "Todo List Site"}
				];
				this.updateCache();
			};

			this.getRemoteActions = function () {
				// TODO: ajax and shit

				// TODO: Remove test
				var newActions = [];
				/* *
				newActions.push({
					type: 'addProject', 
					context: {id: 3, name: 'Polygame'},
					timestamp: Date.now(),
					token: this.getNextToken()
				});
				/* */
				this.applyActions(newActions);
			};

			/**
			 * Apply the given actions to the state of the client.
			 * Generally, these actions will be from the server when the client needs to catch-up.
			 */
			this.applyActions = function (actions) {
				for (var a in actions) {
					var action = actions[a];
					if (action.type === 'addProject') {
						this.addProject(action.context, false);
					}
				}
			};

			/**
			 * Takes the earliest action(s) and sends them to the server.
			 */
			this.publishActions = function () {
				// TODO: ajax and shit
				var action = actionQueue.shift();
			};

			/**
			 * Records the action and retrieves the next token.
			 */
			this.recordAction = function (action, timestamp) {
				actionQueue.push({
					timestamp: timestamp,
					token: currentToken,
					action: action
				});

				// TODO: Ajax
				currentToken = this.getNextToken();
			};

			/**
			 * Retrieves the next token from the server.
			 */
			var tokenId = 0;
			this.getNextToken = function () {
				// TODO: Ajax
				return tokenId++;
			}
		});
	}
);
